import React from 'react';
import PageHeader from '../components/global-components/page-header';
import PricingSection from '../components/section-components/pricing';

const Pricing = () => {
  return (
    <div>
      <PageHeader headertitle="Pricing" />
      <PricingSection />
    </div>
  )
}

export default Pricing

