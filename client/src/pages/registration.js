import React from 'react';
import PageHeader from '../components/global-components/page-header';
import RegistraionSection from '../components/section-components/registration';

const Registration = () => {
  return (
    <div>
      <PageHeader headertitle="Registetion" />
      <RegistraionSection />
    </div>
  )
}

export default Registration

