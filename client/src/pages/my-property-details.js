import React from 'react';
import PageHeader from '../components/global-components/page-header';
import MyPropertyDetailsSection from '../components/section-components/my-property-details';
import { useTranslation } from 'react-i18next';

const PropertyDetails = () => {
  const { t } = useTranslation();
  
  return (
    <div>
      <PageHeader headertitle={t('property_details')} />
      <MyPropertyDetailsSection />
    </div>
  )
}

export default PropertyDetails

