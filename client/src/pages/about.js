import React from 'react';
import PageHeader from '../components/global-components/page-header';
import Mission from '../components/section-components/mission';
import AboutUs from '../components/section-components/about-us';
import ServiceTwo from '../components/section-components/service-two';
import Team from '../components/section-components/team';
import Client from '../components/section-components/client';
import { useTranslation } from 'react-i18next';

const About = () => {
  const { t } = useTranslation();

  return (
    <div>
      <PageHeader headertitle={t('about')} />
      <Mission />
      <AboutUs />
      <ServiceTwo />
      <Team />
      <Client />
    </div>
  )
}

export default About

