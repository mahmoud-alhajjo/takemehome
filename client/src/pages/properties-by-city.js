import React from 'react';
import { PropertiesSection } from '../containers';
import { useParams } from 'react-router-dom';

const PropertiesByCity = () => {
  const { cityId } = useParams();

  return (
    <PropertiesSection
      filters={{ cityId }}
    />
  )
}

export default PropertiesByCity

