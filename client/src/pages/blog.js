import React, { useState } from 'react';
import PopularPost from '../components/blog-components/popular-post';
import PostList from '../components/blog-components/post-list';

const Blog = () => {
  const [search, setSearch] = useState('');

  return (
    <>
      <PopularPost onChange={setSearch} />
      <PostList search={search} />
    </>
  )
}

export default Blog

