import React from 'react';
import { PropertiesSection } from '../containers';
import { useParams } from 'react-router-dom';

const PropertiesByNeighborhood = () => {
  const { neighbourhoodId, cityId } = useParams();

  return (
    <PropertiesSection
      filters={{ neighbourhoodId, cityId }}
    />
  )
}

export default PropertiesByNeighborhood

