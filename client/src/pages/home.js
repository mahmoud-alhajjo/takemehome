import React, { lazy, Suspense } from 'react';
import Banner from '../components/section-components/banner';

const ExploreCities = lazy(() => import('../components/section-components/explore-cities'));
const ExploreNeighborhoods = lazy(() => import('../components/section-components/explore-neighborhoods'));
const FeaturedProperties = lazy(() => import('../components/section-components/featured-properties'));
const Ads = lazy(() => import('../components/section-components/ads'));

const Home = () => {
  return (
    <div>
      <Banner />
      <div className="sub-page">
        <Suspense fallback={() => 'loading...'}>
          <ExploreCities />
          <Ads />
          <FeaturedProperties />
          <ExploreNeighborhoods />
        </Suspense>
      </div>
    </div>
  )
}

export default Home

