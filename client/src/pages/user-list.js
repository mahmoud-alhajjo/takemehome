import React from 'react';
import PageHeader from '../components/global-components/page-header';
import UserListSection from '../components/section-components/user-list';

const UserList = () => {
  return (
    <div>
      <PageHeader headertitle="User List" />
      <UserListSection />
    </div>
  )
}

export default UserList

