import React from 'react';
import EditPropertySection from '../components/section-components/edit-property';

const EditProperty = () => {
  return (
    <EditPropertySection />
  )
}

export default EditProperty
