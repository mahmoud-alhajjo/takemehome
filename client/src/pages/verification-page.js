import React, { useEffect } from 'react'
import { Alert } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { useVerification } from '../common';
import LoaderOverlay from '../components/new-components/LoaderOverlay';
import { loginAction } from '../store';

const VerificationPage = () => {
  const { code, email } = useParams();
  const history = useHistory();
  const dispatch = useDispatch();
  const { userInfo } = useSelector(state => state.auth);

  const [{ loading, data, error }, verifyAccount] = useVerification();

  useEffect(() => {
    verifyAccount({
      data: {
        confirmCode: code,
        email: email || userInfo?.email
      }
    });
  }, [code, userInfo]);

  useEffect(() => {
    if (data?.code === 200) {
      dispatch(loginAction({ accessToken: data?.content?.token }));
      history.push('/');
    }
  }, [data, history]);
  
  return loading ? (
    <LoaderOverlay />
  ) : error ? (
    <Alert variant="error">{error?.message || 'Some thing went wrong'}</Alert>
  ) : null
}

export default VerificationPage
