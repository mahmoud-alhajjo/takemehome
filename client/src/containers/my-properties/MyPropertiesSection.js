import React, { useEffect } from 'react';
import { useAdminProperties } from '../../common';
import { PlaceholderSection } from '../../common/components';
import { EmptyPlaceholder, SinglePropertyHorizontal, SinglePropertyVertical } from '../../components/new-components';
import { useTranslation } from 'react-i18next';

const MyPropertiesSection = ({
  title,
  vertical = true,
  getProperties = () => null
}) => {
  const { t } = useTranslation();
  const [{ data: { content: { data: properties } = {} } = {}, loading }, refetch] = useAdminProperties();

  useEffect(() => {
    getProperties(properties);
  }, [properties, getProperties]);

  return (
    <div className="properties-area pd-top-92">
      <div className="container">
        {title &&
          <div className="section-title">
            <h2 className="title">{title}</h2>
          </div>}
        <div className="row">
          {loading ? <PlaceholderSection vertical={vertical} /> : 
            properties?.length === 0 ? <EmptyPlaceholder text={t('no_my_properties')} btn={{ url: '/add-property', text: t('add_property') }} /> :
            properties?.map((item, i) => vertical ? (
              <SinglePropertyVertical key={i} item={item} detailsUrlPrefix={'/my-properties'} withActions onDelete={() => refetch()} />
            ) : (
              <SinglePropertyHorizontal key={i} item={item} detailsUrlPrefix={'/my-properties'} withActions onDelete={() => refetch()} />
            ))}
        </div>
      </div>
    </div>
  )
}

export default MyPropertiesSection