import React, { useEffect } from 'react';
import { useProperties } from '../../common';
import { toQueryStringObject } from '../../components/helpers';
import { PlaceholderSection } from '../../common/components';
import { EmptyPlaceholder, Pager, SinglePropertyHorizontal, SinglePropertyVertical } from '../../components/new-components';

const PropertiesSection = ({
  title,
  key,
  filters = {},
  vertical = true,
  getProperties = () => null
}) => {

  const [{ data: { content: { data: properties, paginate = {} } = {} } = {}, loading }, refetch] = useProperties({
    params: toQueryStringObject({ filters }, 'filters')
  });

  useEffect(() => {
    getProperties(properties);
  }, [properties, key, getProperties]);

  return (
    <div className="properties-area pd-top-92">
      <div className="container">
        {title &&
          <div className="section-title">
            <h2 className="title">{title}</h2>
          </div>}
        <Pager
          refetch={page => refetch({
            params: toQueryStringObject({
              filters,
              page
            })
          })}
          currentPage={paginate.currentPage}
          lastPage={paginate.lastPage}
        >
          <div className="row">
            {loading ? <PlaceholderSection vertical={vertical} /> :
              properties?.length === 0 ? <EmptyPlaceholder /> :
                properties?.map((item, i) => vertical ? (
                  <SinglePropertyVertical key={i} item={item} />
                ) : (
                    <SinglePropertyHorizontal key={i} item={item} />
                  ))}
          </div>
        </Pager>
      </div>
    </div>
  )
}

export default PropertiesSection