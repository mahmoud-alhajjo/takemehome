import React, { useEffect } from 'react';
import { useArticles } from '../../common';
import { toQueryStringObject } from '../../components/helpers';
import { PlaceholderSection } from '../../common/components';
import { EmptyPlaceholder, SingleArticleHorizontal, SingleArticleVertical } from '../../components/new-components';
import Carousel from '@brainhubeu/react-carousel';

const ArticlesSection = ({
  title,
  key,
  filters = {},
  vertical = true,
  getArticles = () => null,
  withCarousel = false
}) => {

  const [{ data: { content: { data: articles } = {} } = {}, loading }] = useArticles({
    params: toQueryStringObject({ filters }, 'filters')
  });

  useEffect(() => {
    getArticles(articles);
  }, [articles, key, getArticles]);

  return !withCarousel ? (
    <div className="property-news-area pd-top-100 pd-bottom-70">
      <div className="container">
        {title &&
          <div className="section-title">
            <h2 className="title">{title}</h2>
          </div>}
        <div className="row">
          {loading ? <PlaceholderSection vertical={vertical} /> :
            articles?.length === 0 ? <EmptyPlaceholder /> :
              articles?.map((item, i) => vertical ? (
                <SingleArticleVertical key={i} item={item} />
              ) : (
                  <SingleArticleHorizontal key={i} item={item} />
                ))}
        </div>
      </div>
    </div>
  ) : (
      <Carousel
        slidesPerPage={(loading || articles?.length === 0) ? 1 : 4}
      >
        {loading ? <PlaceholderSection vertical={vertical} /> :
          articles?.length === 0 ? <EmptyPlaceholder /> :
            articles?.map((item, i) => vertical ? (
              <SingleArticleVertical key={i} item={item} />
            ) : (
                <SingleArticleHorizontal key={i} item={item} />
              ))}
      </Carousel>
    )
}

export default ArticlesSection