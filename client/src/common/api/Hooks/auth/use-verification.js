import useAxios from 'axios-hooks'
import { Endpoints } from '../../Endpoints';
import { LocalStorage } from '../../../services';

export const useVerification = (config = {}, options = {}) => {
  const respopnse = useAxios({
    url: Endpoints.auth.confirmAccount,
    method: 'POST',
    ...config
  }, {
    manual: true,
    ...options
  });

  const token = respopnse[0]?.data?.content?.token;

  if (token) {
    LocalStorage.set('access_token', token)
  }

  return respopnse;
}