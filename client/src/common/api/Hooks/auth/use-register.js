import useAxios from 'axios-hooks'
import { Endpoints } from '../../Endpoints';
import { LocalStorage } from '../../../services';

export const useRegister = (config = {}, options = {}) => {
  const respopnse = useAxios({
    url: Endpoints.auth.register,
    method: 'POST',
    ...config
  }, {
    manual: true,
    ...options
  });

  const token = respopnse[0]?.data?.content?.token;
  const userInfo = respopnse[0]?.data?.content?.data;

  if (token) {
    LocalStorage.set('access_token', token)
  }
  if (userInfo) {
    LocalStorage.set('user_info', userInfo)
  }

  return respopnse;
}
