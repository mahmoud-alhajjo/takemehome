import useAxios from "axios-hooks"
import { Endpoints } from "../../Endpoints"
import { toQueryStringObject } from "../../../../components/helpers";

export const useAdminProperties = (config = {}, options = {}) => {
  const response = useAxios({
    params: config.id ? undefined : toQueryStringObject({
      perPage: 70,
      page:1,
      filters: {},
      orderBy: "id",
      orderByDirection: "desc"
    }),
    ...config,
    url:  config.id ? `${Endpoints.admin.properties}/${config.id}` : Endpoints.admin.properties,
  }, { useCache: false , ...options });

  return response;
}
