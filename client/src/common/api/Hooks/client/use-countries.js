import useAxios from 'axios-hooks'
import { Endpoints } from "../../Endpoints"

export const useCountries = () => {
  const [{ 
    data: { content: { data: countries = [] } = {} } = {}, 
    loading, 
    error 
  }] = useAxios(Endpoints.client.countries);

  return {
    data: countries?.map(country => selectOptionFormatter(country)),
    loading,
    error
  }
}

const selectOptionFormatter = ({ id, name, code }) => {
  return {
    label: name,
    value: id,
    code
  }
}
