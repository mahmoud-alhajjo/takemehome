import useAxios from "axios-hooks"
import { Endpoints } from "../../Endpoints"

export const useProperties = (config = {}, options = {}) => {
  const response = useAxios({
    url: config.id ? `${Endpoints.client.properties}/${config.id}` : Endpoints.client.properties,
    ...config
  }, { useCache: false, ...options });

  return response[0].error || response[0].data?.code > 300 ? [{}] : response;
}
