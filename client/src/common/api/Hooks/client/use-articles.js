import useAxios from "axios-hooks"
import { Endpoints } from "../../Endpoints"

export const useArticles = (config = {}, options = {}) => {
  const response = useAxios({
    url: config.id ? `${Endpoints.client.articles}/${config.id}` : Endpoints.client.articles,
    ...config
  }, { useCache: false, ...options });

  return response[0].error || response[0].data?.code > 300 ? [{}] : response;
}
