import API from "../Middleware"
import { Endpoints } from "../Endpoints"

export const getArticals = (params, callback = console.log) => {
    callback({ loading: true })
    API.get(Endpoints.admin.articles, { params })
    .then(res => callback({ loading: false, data: res.data }))
    .catch(err => callback({ loading: false, error: err }));
}