import axios from 'axios';
import { Endpoints } from './Endpoints';
import { configure } from 'axios-hooks'
import cogoToast from 'cogo-toast';
import { LocalStorage } from '../services';
import store, { logoutAction, showAuthModalAction } from '../../store';

export const API = axios.create({
  baseURL: Endpoints.baseUrl
})

API.interceptors.request.use(request => {
  const accessToken = LocalStorage.get('access_token');
  
  if (accessToken) {
    request.headers.Authorization = `Bearer ${accessToken}`
  }

  return request;
});

API.interceptors.response.use(response => {
  if (
    response.status === 200 && 
    response.data?.code < 300 && 
    response.config?.method === 'post'
  ) {
    cogoToast.success('Success', {
      position: 'bottom-center',
      hideAfter: 3
    });
  }
  
  if (response.data?.code >= 300) {
    cogoToast.error(response.data?.message ?? 'Something went wrong!', {
      position: 'bottom-center',
      hideAfter: 5
    });
  }

  return response;
}, error => {
  
  if (error?.response?.status === 401) {
    store.dispatch(logoutAction());
    store.dispatch(showAuthModalAction({ showAuthModal: true }));
    cogoToast.error('You need to Login/Signup', {
      position: 'bottom-center',
      hideAfter: 5
    });
  }

  return error;
});

configure({
  axios: API
})

export default API;