import React from 'react'
import { Form } from 'react-bootstrap'
import { Controller } from 'react-hook-form'
import classNames from 'classnames'

const Field = ({
 name,
 placeholder,
 className,
 component: Component = Form.Control,
 errors = {},
 control,
 rules = {},
 as = "",
 defaultValue = "",
 ...rest
}) => {
  if (as) Component = <Component as={as} />
  
  rest = rest.options ? {
    ...rest,
    classNamePrefix: rest.options ? "react-select" : undefined
  } : rest;
  
  return (
    <Form.Group controlId={name} className={classNames("form-field", className)}>
      <Controller
        name={name}
        className={classNames({ "is-invalid": errors[name], "react-select": !!rest.options })}
        as={Component}
        isInvalid={errors[name]}
        placeholder={placeholder}
        control={control}
        rules={rules}
        defaultValue={defaultValue}
        {...rest}
      />
      {errors[name] &&
        <Form.Control.Feedback type="invalid">{errors[name]?.message}</Form.Control.Feedback>}
    </Form.Group>
  )
}

export default Field
