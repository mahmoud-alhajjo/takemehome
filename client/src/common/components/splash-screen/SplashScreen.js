import React from 'react'
import { Spinner } from 'react-bootstrap'

import './splash-screen.scss'

const SplashScreen = () => {
  return (
    <div className="splash-screen">
      <Spinner 
        animation="grow" 
        variant="light" 
        className="splash-screen--loader"
      />
      <Spinner 
        animation="grow" 
        variant="light" 
        className="splash-screen--loader"
      />
      <Spinner 
        animation="grow" 
        variant="light" 
        className="splash-screen--loader"
      />
    </div>
  )
}

export default SplashScreen
