import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Button } from 'react-bootstrap'
import classNames from 'classnames'
import $ from 'jquery'

const onLanguageChange = newLang => {
  const otherLanguage = newLang === 'en' ? 'ar' : 'en';
  const direction = newLang === 'en' ? 'ltr' : 'rtl';
  const otherDirection = direction === 'ltr' ? 'rtl' : 'ltr';

  localStorage.setItem('language', otherLanguage);
  $('html').attr('lang', otherLanguage);
  $('body').addClass(otherDirection).removeClass(direction);
}

const LanguageSwitch = ({ lang }) => {
  const { i18n } = useTranslation();
  const currentLanguage = i18n.language;
  const otherLanguage = currentLanguage === 'en' ? 'ar' : 'en';
  const label = currentLanguage === 'ar' ? 'English' : 'عربي';
  const className = currentLanguage === 'ar' ? '' : 'arabic-font';

  const handleLanguageChange = () => {
    i18n.changeLanguage(otherLanguage, () => {
      onLanguageChange(currentLanguage);
    });
  }

  useEffect(() => {
    const direction = currentLanguage === 'en' ? 'ltr' : 'rtl';
    const otherDirection = direction === 'ltr' ? 'rtl' : 'ltr';

    localStorage.setItem('language', currentLanguage);
    $('html').attr('lang', currentLanguage);
    $('body').addClass(direction).removeClass(otherDirection);
  }, []);

  return (
    <Button 
      onClick={handleLanguageChange}
      variant="dark"
      className={classNames("language-switch", className, lang)}
    >{label}</Button>
  )
}

export default LanguageSwitch
