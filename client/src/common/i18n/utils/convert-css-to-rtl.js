var glob = require('glob');
var rtlcss = require('rtlcss');
var fs = require('fs');

glob("**/build/**/*.css", function(err, files) {
    if (err) throw err;
    files.map(cssFilePath => {
        generateRtlCssFiles(cssFilePath);
    })
});

function generateRtlCssFiles(cssFilePath) {
    fs.readFile(cssFilePath, function(err, cssFileContent) {
        if (err) throw err;
        cssToRtl(cssFileContent, cssFilePath);
    })
}

function cssToRtl(css, filepath) {
    var extraCss = `
    /*rtl:begin:ignore*/
    [dir="rtl"] {
        direction: rtl !important;
    }
    /*rtl:end:ignore*/
    `
    var rtlCssContent = rtlcss.process(css);
    writeCssToFile(rtlCssContent, filepath);
}

function writeCssToFile(content, filepath) {
    fs.writeFile(filepath, content, {}, function(err) {
        if (err) throw err;
        console.info('RTL Css file generated successfully!');
    })
}