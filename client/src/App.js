import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from './pages/home';
import Property from './pages/property';
import PropertiesByCity from './pages/properties-by-city';
import PropertyDetails from './pages/property-details';
import Error from './pages/error';
import Blog from './pages/blog';
import VerificationPage from './pages/verification-page';
import BlogDetails from './pages/blog-details';
import SearchList from './pages/search-list';
import PropertiesByNeighborhood from './pages/properties-by-neighborhood';
import AddNew from './pages/add-property';
import MyProperties from './pages/my-properties';
import { LocaleProvider } from './common';
import { AuthModal } from './components/new-components';
import { useSelector, useDispatch } from 'react-redux';
import { loginAction } from './store';
import { AppRoute } from './components';
import { useTranslation } from 'react-i18next';
import ExploreNeighborhoods from './components/section-components/explore-neighborhoods';
import ExploreCities from './components/section-components/explore-cities';
import MyPropertyDetails from './components/section-components/my-property-details';
import EditProperty from './pages/edit-property';

import "./assets/scss/style.scss";

const App = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);

  const handleAuthSuccess = (data) => {
    dispatch(loginAction({ accessToken: data?.content?.token, userInfo: data?.content?.data }));
  }

  return (
    <LocaleProvider>
      <Router basename="/">
        <Switch>
          <AppRoute exact path="/" component={Home} />
          <AppRoute exact path="/search" component={SearchList} title={t('search')} />
          <AppRoute exact path="/property" component={Property} title={t('properties')} />
          <AppRoute exact path="/my-properties" component={MyProperties} title={t('my_properties')} />
          <AppRoute exact path="/my-properties/:propertyId" component={MyPropertyDetails} title={t('property_details')} />
          <AppRoute exact path="/property/city/:cityId?" component={PropertiesByCity} title={t('properties_by_cities')} />
          <AppRoute exact path="/property/neighborhood/:neighbourhoodId?/:cityId?" component={PropertiesByNeighborhood} title={t('properties_by_neighborhoods')} />
          <AppRoute exact path="/property/:propertyId" component={PropertyDetails} title={t('property_details')} />
          <AppRoute exact path="/neighborhoods" component={ExploreNeighborhoods} title={t('neighborhoods')} />
          <AppRoute exact path="/cities" component={ExploreCities} title={t('cities')} />
          <AppRoute exact path="/add-property" component={AddNew} title={t('add_property')} />
          <AppRoute exact path="/edit-property/:propertyId" component={EditProperty} title={t('edit_property')} />

          <AppRoute exact path="/blog" component={Blog} title={t('blog')} />
          <AppRoute exact path="/blog/:articleId" component={BlogDetails} title={t('article_details')} />

          <AppRoute exact path="/verify/:code/:email?" component={VerificationPage} title={t('verify_email')} />

          <Route path={["/error", "*"]} component={Error} />
        </Switch>
      </Router>
      <AuthModal show={auth.showAuthModal} onSuccess={handleAuthSuccess} />
    </LocaleProvider>
  )
}

export default App;
