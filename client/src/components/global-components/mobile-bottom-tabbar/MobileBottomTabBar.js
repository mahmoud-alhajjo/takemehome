import React from 'react'
import { Nav } from 'react-bootstrap'
import { Link, useLocation } from 'react-router-dom'

const MobileBottomTabBar = ({
  tabs = [],
  ...props
}) => {
  const { pathname } = useLocation();

  return (
    <Nav className="mobile-bottom-tabbar" variant="tabs" defaultActiveKey="/" {...props}>
      {
        tabs.map(tab => {
          return (
            <Nav.Item key={tab.key}>
              <Nav.Link as={Link} active={pathname === tab.url} to={tab.url}>
                <i className={tab.icon} />
                {tab.label && <span>{tab.label}</span>}  
              </Nav.Link>
            </Nav.Item>
          )
        })
      }
    </Nav>
  )
}

export default MobileBottomTabBar
