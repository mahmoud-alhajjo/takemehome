import React from 'react'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import withMobileHeader from './withMobileHeader'

const MobileHeader = ({
  title,
  image,
  url = '/',
  onMenuClick = () => {},
  onProfileClick = () => {},
  menuClassName,
  profileClassName,
  titleClassName
}) => {
  return (
    <div className="mobile-header">
      <div className={classNames("mobile-header--left", menuClassName)}>
        <i className="fa fa-reorder" onClick={onMenuClick} />
      </div>
      <div className={classNames("mobile-header--center", titleClassName)}>
        <Link to={url}>
          {title && <h5 className="mobile-header--title">{title}</h5>}
          {image && <img src={image} height={40} alt="title" className="mobile-header--image" />}
        </Link>
      </div>
      <div className={classNames("mobile-header--right", profileClassName)}>
        <i className="fa fa-plus" onClick={onProfileClick} />
      </div>
    </div>
  )
}

export default withMobileHeader(MobileHeader)
