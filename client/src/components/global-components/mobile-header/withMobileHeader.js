import React, { useState, useEffect } from 'react'
import Sidebar from 'react-sidebar'
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

const withMobileHeader = Component => ({
  sideBar: sidebarContent = () => {},
  ...props
}) => {
  const history = useHistory();
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const { i18n } = useTranslation();

  useEffect(() => {
    document.querySelector('body').style = sidebarOpen ? 'overflow-y: hidden; max-height: 100vh;' : '';
  }, [sidebarOpen]);

  const onLinkClick = () => {
    setSidebarOpen(prev => !prev);
  }

  const onAddProperty = () => {
    history.push('/add-property')
  }

  return (
    <>
      <Component
        onMenuClick={() => setSidebarOpen(prev => !prev)}
        onProfileClick={() => onAddProperty()}
        {...props}
      />
      <Sidebar
        sidebar={sidebarContent({ onLinkClick })}
        open={sidebarOpen}
        onSetOpen={setSidebarOpen}
        styles={sidebarStyle}
        pullRight={i18n.language === 'ar'}
      >
        {''}
      </Sidebar>
    </>
  )
}

const sidebarStyle = {
  sidebar: { 
    background: "white",
    width: '90%',
    paddingTop: 54,
    paddingBottom: 54 + 25
  }
}

export default withMobileHeader
