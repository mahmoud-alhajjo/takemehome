import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

const PageHeader = ({ headertitle, subheader }) => {
  const { t } = useTranslation();
  
  const inlineStyle = {
    backgroundImage: 'url(/assets/img/bg/4.jpg)'
  }

  return (
    <div className="breadcrumb-area jarallax page-header-container" style={inlineStyle}>
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="breadcrumb-inner">
              <h1 className="page-title">{headertitle}</h1>
              <ul className="page-list">
                <li className="mr-1"><Link to="/">{t('home')}</Link></li>
                <li className="ml-1">{subheader || headertitle}</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PageHeader
