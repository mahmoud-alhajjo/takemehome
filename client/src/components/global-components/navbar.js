import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { LanguageSwitch, i18n } from '../../common';
import { Button } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { logoutAction, showAuthModalAction } from '../../store';
import ProfileHeader from '../new-components/ProfileHeader';

const Navbar = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { accessToken: isAuthenticated, userInfo } = useSelector(state => state.auth);
  const [bgColor, setBgColor] = useState('linear-gradient(180deg, #ffffff9c, transparent)');

  const handleLogin = () => {
    dispatch(showAuthModalAction({ showAuthModal: true }))
  }

  const handleLogout = (e) => {
    e.preventDefault();
    dispatch(logoutAction());
  }

  useEffect(() => {
    const onScroll = (event) => {
      if (window.scrollY === 0) {
        setBgColor('linear-gradient(180deg, #ffffff9c, transparent)');
      } else if (window.scrollY > 0 && bgColor === 'linear-gradient(180deg, #ffffff9c, transparent)') {
        setBgColor('white');
      }
    }

    window.addEventListener('scroll', onScroll);
    // ComponentDidUnmount
    return () => {
      window.removeEventListener('scroll', onScroll);
    }
  }, [bgColor]);

  return (
    <div className="navbar-area">
      <nav className="navbar navbar-area navbar-expand-lg" style={{ background: bgColor, boxShadow: bgColor === 'white' ? '0 3px 10px rgba(0,0,0,.2)' : 'none' }}>
        <div className="container nav-container">
          <div className="responsive-mobile-menu">
            <button className="menu toggle-btn d-block d-lg-none" data-toggle="collapse" data-target="#realdeal_main_menu" aria-expanded="false" aria-label="Toggle navigation">
              <span className="icon-left" />
              <span className="icon-right" />
            </button>
          </div>
          <div className="logo readeal-top">
            <Link to="/"><img src={"/logo/takemehome-logo-wide.png"} width={140} alt="logo" /></Link>
          </div>
          <div className="nav-right-part nav-right-part-mobile">
            <Link className="btn btn-yellow" to="/add-property">{t('add_listing')} <span className="right"><i className="la la-plus" /></span></Link>
          </div>
          <div className="collapse navbar-collapse" id="realdeal_main_menu">
            <ul className="navbar-nav menu-open readeal-top">
              <li><Link to="/">{t('home')}</Link></li>
              <li><Link to="/search">{t('search')}</Link></li>
              <li><Link to="/property">{t('properties')}</Link></li>
              <li><Link to="/blog">{t('blog')}</Link></li>
              {isAuthenticated && <li><Link to="/my-properties">{t('my_properties')}</Link></li>}
              {!isAuthenticated && <li><Button variant="light" onClick={handleLogin}>{(userInfo && !isAuthenticated) ? t('verify_email') : t('login')}</Button></li>}
              {/* <li><Link to="/contact">{t('contact_us')}</Link></li> */}
              {/* <li><Link to="/about">{t('about')}</Link></li> */}
            </ul>
          </div>
          <LanguageSwitch lang={i18n.language} />
          <div className="nav-right-part nav-right-part-desktop readeal-top ml-3">
            <Link className="btn btn-yellow" to="/add-property" id="show-install-banner">{t('add_property')} <span className="right"><i className="la la-plus" /></span></Link>
          </div>
          {(!!isAuthenticated || !!userInfo) &&
            <ProfileHeader 
              userInfo={userInfo}
              menu={[
                { key: 'logout', label: t('logout'), onClick: handleLogout }
              ]}
            />}
        </div>
      </nav>
    </div>
  )
}


export default React.memo(Navbar)