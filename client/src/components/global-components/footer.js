import React from 'react';
import { Link } from 'react-router-dom';
import { SocialMediaList } from '../new-components';
import { useTranslation } from 'react-i18next';
import { footerList1, footerList2, footerList3 } from '../config';

const Footer_v1 = () => {
  const { t } = useTranslation();

  return (
    <footer className="footer-area">
      <div className="container">
        <div className="footer-top">
          <div className="row">
            <div className="col-sm-4">
              <Link className="footer-logo" to="/"><img src="/logo/takemehome-logo-wide.png" height={404 / 5} style={{ maxWidth: '100%' }} alt="logo" /></Link>
            </div>
            <div className="col-sm-8">
              <div className="footer-social text-sm-right">
                <span>{t('follow_us')}</span>
                <SocialMediaList />
              </div>
            </div>
          </div>
        </div>
        <div className="footer-bottom">
          <div className="row">
            {/* <div className="col-lg-4 col-sm-6">
              <div className="widget widget_nav_menu">
                <h4 className="widget-title">{t(footerList1.title)}</h4>
                <ul>
                  {
                    footerList1.items.map(item => (
                      <li key={item.text} className="readeal-top"><Link to={item.url}>{t(item.text)}</Link></li>
                    ))
                  }
                </ul>
              </div>
            </div> */}
            <div className="col-lg-4 col-sm-6">
              <div className="widget widget_nav_menu">
                <h4 className="widget-title">{t(footerList2.title)}</h4>
                <ul>
                  {
                    footerList2.items.map(item => (
                      <li key={item.text} className="readeal-top"><Link to={item.url}>{t(item.text)}</Link></li>
                    ))
                  }
                </ul>
              </div>
            </div>
            <div className="col-lg-4 col-sm-6">
              <div className="widget widget_nav_menu">
                <h4 className="widget-title">{t(footerList3.title)}</h4>
                <ul>
                  {
                    footerList3.items.map(item => (
                      <li key={item.text} className="readeal-top"><Link to={item.url}>{t(item.text)}</Link></li>
                    ))
                  }
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="copy-right text-center">{t('copyrights_footer')}</div>
      </div>
    </footer>
  )
}


export default Footer_v1