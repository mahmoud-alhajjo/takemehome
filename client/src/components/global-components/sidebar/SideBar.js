import React from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { showAuthModalAction, logoutAction } from '../../../store';
import { LanguageSwitch } from '../../../common';
import { Nav } from 'react-bootstrap';

const SideBar = ({
  onLinkClick = () => {}
}) => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const { accessToken: isAuthenticated, userInfo } = useSelector(state => state.auth);

  const handleLogin = (e) => {
    e.preventDefault();
    dispatch(showAuthModalAction({ showAuthModal: true }));
    onLinkClick();
  }

  const handleLogout = (e) => {
    e.preventDefault();
    dispatch(logoutAction());
    onLinkClick();
  }

  return (
    <Nav className="sidebar flex-column">
      <h6 className="text-center mt-3">{t('pages')}</h6>
      <Nav.Link onClick={onLinkClick} as={Link} to="/cities" eventKey="cities"><i className="fa fa-map" /> {t('explore_cities')}</Nav.Link>
      <Nav.Link onClick={onLinkClick} as={Link} to="/neighborhoods" eventKey="neighborhoods"><i className="fa fa-map-marker" /> {t('explore_neighborhoods')}</Nav.Link>
      <Nav.Link onClick={onLinkClick} as={Link} to="/add-property" eventKey="add-property"><i className="fa fa-plus" /> {t('add_property')}</Nav.Link>
      <Nav.Link onClick={onLinkClick} as={Link} to="/search" eventKey="search"><i className="fa fa-search" /> {t('search')}</Nav.Link>
      <Nav.Link onClick={onLinkClick} as={Link} to="/property" eventKey="property"><i className="fa fa-building" /> {t('properties')}</Nav.Link>
      <Nav.Link onClick={onLinkClick} as={Link} to="/blog" eventKey="blog"><i className="fa fa-file" /> {t('blog')}</Nav.Link>
      {/* <Nav.Link onClick={onLinkClick} as={Link} to="/contact" eventKey="contact">{t('contact_us')}</Nav.Link>
        <Nav.Link onClick={onLinkClick} as={Link} to="/about" eventKey="about">{t('about')}</Nav.Link> */}
      {isAuthenticated &&
        <Nav.Link onClick={onLinkClick} as={Link} to="/my-properties" eventKey="my-properties"><i className="fa fa-building" /> {t('my_properties')}</Nav.Link>}
      <h6 className="text-center mt-3">{t('settings')}</h6>
      {!isAuthenticated &&
        <Nav.Link onClick={handleLogin} eventKey="login"><i className="fa fa-sign-in" /> {(userInfo && !isAuthenticated) ? t('verify_email') : t('login')}</Nav.Link>}
      {(!!isAuthenticated || userInfo) &&
        <Nav.Link onClick={handleLogout} eventKey="logout"><i className="fa fa-sign-out" /> {t('logout')}</Nav.Link>}
      <h6 className="text-center mt-3">{t('switch_lang_to')}</h6>
      <LanguageSwitch lang={i18n.language} />
    </Nav>
  )
}

export default SideBar
