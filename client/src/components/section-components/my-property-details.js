import React from 'react';
import { useTranslation } from 'react-i18next';
import { useAdminProperties } from '../../common';
import { useParams } from 'react-router-dom';
import { propertyFormatter, imageFallback } from '../helpers';
import { PlaceholderVerticalCard } from '../../common/components';
import { Maps } from '../../containers';
import PropertyDetailsCard from '../new-components/PropertyDetailsCard';

const MyPropertyDetails = () => {
  const { t } = useTranslation();
  const { propertyId } = useParams();
  const [{ data: { content: { data } = {} } = {}, loading }] = useAdminProperties({
    id: propertyId
  });

  const property = propertyFormatter(data || {}, '/property', t);

  return loading ? (
    <div className="d-flex justify-content-center align-items-center h-100 pt-5">
      <PlaceholderVerticalCard />
    </div>
  ) : (
    <PropertyDetailsCard property={property} />
  )
}

export default MyPropertyDetails