import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Filters } from '../new-components';
import { useLocation } from 'react-router-dom';
import qs from 'qs';
import { PropertiesSection } from '../../containers';
import { isMobile } from '../helpers';

const SearchList = () => {
  const { t } = useTranslation();
  const location = useLocation();
  const filterParams = qs.parse(location.search.substring(1));
  const [filters, setFilters] = useState(filterParams);
  const [properties, setProperties] = useState([]);

  return (
    <div className="search-page-wrap pd-top-100 pd-bottom-70">
      <div className="search-container">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xl-3 col-lg-4 sitebar">
              <Filters
                initialFilters={filterParams}
                onChange={setFilters}
              />
            </div>
            <div className="col-xl-8 col-lg-8">
              <div className="row mb-3">
                <div className="col-md-9 col-sm-8">
                  <h6 className="filter-title mt-3 mb-lg-0">{properties?.length ?? 0} {t('properties')}</h6>
                </div>
              </div>
              <PropertiesSection
                vertical={isMobile}
                getProperties={setProperties}
                id={JSON.stringify(filters)}
                filters={{
                  contractType: filters.category,
                  search: filters.title,
                  cityId: filters.city,
                  neighbourhoodId: filters.neighborhood,
                  between: (filters.price || filters.space || filters.bedrooms || filters.bathrooms) ? {
                    price: filters.price ? [0, filters.price] : undefined,
                    space: filters.space ? [0, filters.space] : undefined,
                    bedroomNumber: filters.bedrooms ? filters.bedrooms : [0, 10],
                    bathroomNumber: filters.bathrooms ? filters.bathrooms : [0, 10],
                  } : undefined,
                  // bedroomNumber: filters.bedrooms,
                  // bathroomNumber: filters.bathrooms
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SearchList