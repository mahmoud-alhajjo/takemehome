import React from 'react';
import { socialMediaLinks } from '../config';
import Login from '../new-components/Login';
import Register from '../new-components/Register';

const Registration = () => {
  return (
    <div className="register-page-area pd-bottom-100">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-xl-4 col-lg-5 col-md-6 mb-5 mb-md-0">
            <Login />
          </div>
          <div className="col-xl-4 col-lg-5 col-md-6">
            <Register />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Registration
