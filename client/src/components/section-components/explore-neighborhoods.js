import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useNeighborhoods } from '../../common';
import { useTranslation } from 'react-i18next';
import { images } from '../config';
import { PlaceholderSection } from '../../common/components';
import { EmptyPlaceholder } from '../new-components';
import withSelectOptions from '../new-components/withSelectOptions';
import Select from 'react-select';

const ExploreNeighborhoods = ({ cities }) => {
  const { t, i18n: { language } } = useTranslation();
  const [city, setCity] = useState(1);
  const { data: neighborhoods, loading } = useNeighborhoods({
    perPage: 20,
    filters: {
      cityId: city
    }
  });

  return (
    <div className="explore-area pd-top-85">
      <div className="container">
        <div className="section-title text-center">
          <h2 className="title">{t('explore_neighborhoods')}</h2>
        </div>
        <div className="row">
          <div className="col-12 mb-5">
            <Select
              options={cities.options}
              isLoading={cities.loading}
              onChange={({ value }) => setCity(value)}
              placeholder={t('city')}
            />
          </div>
          {loading ? <PlaceholderSection vertical /> :
            neighborhoods?.length === 0 ? <EmptyPlaceholder /> :
              neighborhoods?.map((neighborhood, i) =>
                <div key={neighborhood.value} className="col-lg-3 col-sm-6">
                  <div className="single-explore">
                    <div className="thumb">
                      <img src={images.cities[neighborhood.cityId]} alt="explore" />
                    </div>
                    <div className="details readeal-top">
                      <h4><Link to={`/property/neighborhood/${neighborhood.value}/${city}`}>{neighborhood.label[language]}</Link></h4>
                      <ul className="list">
                        <li><img src={"/assets/img/icons/1.png"} alt="icona" />{neighborhood.cityName?.[language] || neighborhood.cityName}</li>
                      </ul>
                    </div>
                  </div>
                </div>
              )}

        </div>
      </div>
    </div>
  )
}

export default withSelectOptions(ExploreNeighborhoods)
