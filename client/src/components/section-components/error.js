import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

const Error = () => {
  const { t } = useTranslation();

  return (
    <div>
      <div className="error-page text-center">
        <div className="container">
          <div className="error-page-wrap d-inline-block">
            <Link to="/">{t('go_to_homepage')}</Link>
            <h2>404</h2>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Error