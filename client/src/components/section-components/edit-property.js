import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useAdminProperties } from '../../common';
import { addNewPropertyFormatter, citiesLocations, getPropertyFormatter, removeImagePrefix } from '../helpers';
import { useHistory, useParams } from 'react-router-dom';
import PropertyForm from '../new-components/PropertyForm';
import LoaderOverlay from '../new-components/LoaderOverlay';

const initialValues = {
  category: 'sell',
  price: 100000000,
  space: 100,
  amenities: [],
  locations: citiesLocations.damascus
}

const EditPropertySection = () => {
  const history = useHistory();
  const { propertyId } = useParams();
  const { t } = useTranslation();

  const [{ loading, data }, editProperty] = useAdminProperties({ method: 'POST', id: propertyId }, { manual: true });
  const [{ loading: initialValuesLoading, data: { content: { data: initialValuesData } = {} } = {} }] = useAdminProperties({ id: propertyId });
  const init = getPropertyFormatter(initialValuesData, t);

  const onSubmit = (values) => {
    console.log(values);
    editProperty({
      data: addNewPropertyFormatter({
        ...values,
        _method: 'PUT',
        deletedImages: values.deletedImages?.map(dImg => removeImagePrefix(dImg)) || '[]',
        images: values.images?.filter(img => typeof img !== 'string'),
        location: values.location ? JSON.stringify(values.location) : JSON.stringify(citiesLocations.damascus)
      })
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/my-properties");
    }
  }, [data]);

  return (
    <div className="add-new-property-area pd-top-90 mg-bottom-100">
      {(initialValuesLoading) && 
        <LoaderOverlay />}
      <div className="container">
        <PropertyForm
          onSubmit={onSubmit}
          initialValues={init}
          submitLoading={loading}
        />
      </div>
    </div>
  )
}

export default EditPropertySection
