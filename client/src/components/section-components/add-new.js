import React, { useEffect } from 'react';
import { useAdminProperties } from '../../common';
import { addNewPropertyFormatter, citiesLocations } from '../helpers';
import { useHistory } from 'react-router-dom';
import PropertyForm from '../new-components/PropertyForm';
import { priceSellMax, spaceMax } from '../config';

const initialValues = {
  category: 'sell',
  price: priceSellMax,
  space: spaceMax,
  amenities: []
}

const AddNew = () => {
  const history = useHistory();

  const [{ loading, data }, addProperty] = useAdminProperties({ method: 'POST' }, { manual: true })

  const onSubmit = (values) => {
    addProperty({
      data: addNewPropertyFormatter({
        ...values,
        location: values.location ? JSON.stringify(values.location) : JSON.stringify(citiesLocations.damascus)
      })
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/my-properties");
    }
  }, [data, history])

  return (
    <div className="add-new-property-area pd-top-90 mg-bottom-100">
      <div className="container">
        <PropertyForm
          onSubmit={onSubmit}
          initialValues={initialValues}
          submitLoading={loading}
        />
      </div>
    </div>
  )
}

export default AddNew
