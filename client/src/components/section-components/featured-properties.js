import React from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import { PropertiesSection } from '../../containers';

const Featured = ({
  Customclass
}) => {
  const { t } = useTranslation();
  Customclass = Customclass ? Customclass : 'pd-top-60'

  return (
    <div className={classNames("featured-area", Customclass)}>
      <div className="container">
        <div className="section-title text-center">
          <h2 className="title">{t('recent_properties')}</h2>
        </div>
        <div className="row justify-content-center">
          <PropertiesSection />
        </div>
      </div>
    </div>
  )
}

export default Featured