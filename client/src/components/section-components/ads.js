import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

const Ads = () => {
  const { t } = useTranslation();

  const inlineStyle = {
    backgroundImage: 'url(/assets/img/bg/1.png)'
  }

  return (
    <div className="call-to-action-area pd-top-70">
      <div className="container">
        <div className="call-to-action" style={inlineStyle}>
          <div className="cta-content">
            <h3 className="title">{t('ad_title')}</h3>
            <Link className="btn btn-white" to="/add-property">{t('add_property')}</Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Ads