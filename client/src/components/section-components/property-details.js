import React from 'react';
import { useTranslation } from 'react-i18next';
import { useProperties } from '../../common';
import { useParams } from 'react-router-dom';
import { propertyFormatter } from '../helpers';
import { PlaceholderVerticalCard } from '../../common/components';
import PropertyDetailsCard from '../new-components/PropertyDetailsCard';
import { Helmet } from 'react-helmet';

const PropertyDetails = () => {
  const { t } = useTranslation();
  const { propertyId } = useParams();
  const [{ data: { content: { data } = {} } = {}, loading }] = useProperties({
    id: propertyId
  });

  const property = propertyFormatter(data || {}, '/property', t);

  return (
    <>
      <Helmet titleTemplate={`%s | ${t('sitename')}`}>
        <title>{property.title}</title>
        <meta name="description" content={property.description}/>
        <meta name="author" content={property.authorname}/>
        <meta name="keyword" content={property.description}/>
        <meta property="og:title" content={property.title} />
        <meta property="og:site_name" content={t('sitename')}/>
        <meta property="og:description" content={property.description} />
        <meta property="og:image" content={property.image} />
      </Helmet>
      {loading ? (
        <div className="d-flex justify-content-center align-items-center h-100 pt-5">
          <PlaceholderVerticalCard />
        </div>
      ) : (
          <PropertyDetailsCard property={property} />
        )}
    </>
  )
}

export default PropertyDetails