import React from 'react';
import FiltersHorizontal from '../new-components/FiltersHorizontal';
import { useTranslation } from 'react-i18next';
import MobileDetect from 'mobile-detect';
import { isMobile } from '../helpers';

const Banner = () => {
  const { t } = useTranslation();

  const inlineStyle = {
    backgroundImage: 'url(/assets/img/banner/3.png)',
    backgroundPosition: isMobile ? 'right' : 'center'
  }

  return (
    <div className="banner-area jarallax" style={inlineStyle}>
      <div className="container">
        <div className="banner-inner-wrap w-100">
          <div className="row no-gutters w-100">
            <div className="col-12">
              <div className="banner-inner">
                <h5 className="sub-title">{t('intro_subtitle')}</h5>
                <h1 className="title mb-5">{t('intro_title')}</h1>
              </div>
            </div>
            {
              !new MobileDetect(window.navigator.userAgent).mobile() &&
              <div className="col-12">
                <FiltersHorizontal />
              </div>
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default Banner