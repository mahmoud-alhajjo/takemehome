import React from 'react';
import SimpleCardOverlay from '../new-components/SimpleCardOverlay';
import { images } from '../config';
import { i18n, useCities } from '../../common';
import { range } from 'ramda';
import { useTranslation } from 'react-i18next';
import { PlaceholderSection } from '../../common/components';
import { EmptyPlaceholder } from '../new-components';

const ExploreCities = () => {
  const { t, i18n } = useTranslation();
  const { data: cities, loading } = useCities();

  const inlineStyle = {
    backgroundImage: 'url(/assets/img/bg/2.png)'
  }

  return (
    <div className="city-intro-area pd-bottom-70" style={inlineStyle}>
      {/* city area start */}
      <div className="city-area pd-top-92">
        <div className="container">
          <div className="section-title text-center">
            <h2 className="title">{t('explore_cities')}</h2>
          </div>
          <div className="row justify-content-center align-items-center h-100">
            <div className="city-sizer col-1" />
            {loading ? <PlaceholderSection square /> :
              cities?.length === 0 ? <EmptyPlaceholder /> :
                cities?.map((item, i) =>
                  <SimpleCardOverlay
                    key={i}
                    item={{
                      class: ["col-lg-3 col-sm-6", "col-lg-2 col-sm-6"][range(0, 1)],
                      sectionclass: ["sc-one", "sc-two", "sc-three"][range(0, 2)],
                      image: images.houses[item.value],
                      title: item.label?.[i18n.language],
                      url: `/property/city/${item.value}`,
                      content: item.countryName?.[i18n.language] || t(item.countryName)
                    }}
                  />
                )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default ExploreCities