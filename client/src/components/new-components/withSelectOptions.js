import React, { useState } from 'react'
import { useTranslation } from 'react-i18next';
import { useCities, useCountries, useNeighborhoods } from '../../common';
import { bathroomsOptions, bedroomsOptions, formatOptionsWithLang } from '../config';

const withSelectOptions = Component => ({
  ...props
}) => {
  const { t, i18n } = useTranslation();
  const [city, setCity] = useState();
  
  const { data: countries, loading: loadingCountries } = useCountries();
  const countriesOptions = formatOptionsWithLang(countries, i18n.language);

  const { data: cities, loading: loadingCities } = useCities();
  const citiesOptions = formatOptionsWithLang(cities, i18n.language);

  const { data: neighborhoods, loading: loadingNeighborhoods } = useNeighborhoods({ perPage: 10000, filters: city ? { cityId: city } : undefined });
  const neighborhoodsOptions = formatOptionsWithLang(neighborhoods, i18n.language);

  return (
    <Component
      {...props}
      onChange={setCity}
      countries={{ options: countriesOptions, loading: loadingCountries }}
      cities={{ options: citiesOptions, loading: loadingCities }}
      neighborhoods={{ options: neighborhoodsOptions, loading: loadingNeighborhoods }}
      bedrooms={{ options: bedroomsOptions(t), loading: false }}
      bathrooms={{ options: bathroomsOptions(t), loading: false }}
    />
  )
}

export default withSelectOptions
