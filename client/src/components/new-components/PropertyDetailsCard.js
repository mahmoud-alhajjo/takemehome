import React from 'react'
import { imageFallback } from '../helpers';
import { Maps } from '../../containers';
import { useTranslation } from 'react-i18next';
import ImageGallery from 'react-image-gallery';

const PropertyDetailsCard = ({
  property
}) => {
  const { t, i18n } = useTranslation();

  return (
    <div className="property-details-area">
      <div className="bg-gray pd-top-100 pd-bottom-90">
        <div className="container">
          <div className="row ">
            <div className="col-xl-9 col-lg-8">
              <div className="property-details-slider">
                <div className="item">
                  <div className="thumb">
                    {property.images && property.images?.length > 0 ? (
                      <ImageGallery
                        items={property.images?.map?.(img => ({ original: img, thumbnail: img }))}
                        showThumbnails={property.images?.length > 1}
                        showPlayButton={property.images?.length > 1}
                        isRTL={i18n.language === 'ar'}
                        onImageError={imageFallback}
                      />
                    ) : (
                        <img src={property.image} alt="news" onError={imageFallback} />
                      )}
                  </div>
                </div>
              </div>
              <div className="property-details-slider-info">
                <h3><span>{property.newerprice}</span> {property.title}</h3>
                <h6><span className="badge badge-info badge-pill" style={{ fontSize: 14 }}>{t(property.category)}</span></h6>
              </div>
            </div>
            <div className="col-xl-3 col-lg-4">
              <div className="widget widget-owner-info mt-lg-0 mt-5">
                <div className="owner-info text-center">
                  <div className="thumb">
                    <img src={property.icon} width={60} alt="news" />
                  </div>
                  <div className="details">
                    <h6>{property.authorname}</h6>
                    <span className="designation">{t('publisher')}</span>
                    <p className="reviews"><i className="fa fa-star" /><span>{property.rate}</span> {property.views} {t('view')}</p>
                  </div>
                </div>
                <div className="contact-info">
                  <h6 className="mb-3">{t('contact_info')}</h6>
                  <div className="media">
                    <div className="media-left">
                      <i className="fa fa-phone" />
                    </div>
                    <div className="media-body">
                      <p>{t('phone')}</p>
                      <span>{property.phone}</span>
                    </div>
                  </div>
                  <div className="media mb-0">
                    <div className="media-left">
                      <i className="fa fa-envelope" />
                    </div>
                    <div className="media-body">
                      <p>{t('email')}</p>
                      <span>{property.email}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row pd-top-90">
          <div className="col-lg-9">
            <div className="property-info mb-5">
              <div className="row">
                <div className="col-md-3 col-sm-6">
                  <div className="single-property-info">
                    <h5>{t('bedrooms')}</h5>
                    <p><i className="fa fa-bed" />{property.features[0].title}</p>
                  </div>
                </div>
                <div className="col-md-3 col-sm-6">
                  <div className="single-property-info">
                    <h5>{t('bathrooms')}</h5>
                    <p><i className="fa fa-bath" />{property.features[1].title}</p>
                  </div>
                </div>
                <div className="col-md-3 col-sm-6">
                  <div className="single-property-info">
                    <h5>{t('area')}</h5>
                    <p><img src={"/assets/img/icons/7.png"} alt="news" />{property.area}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="property-news-single-card style-two border-bottom-yellow">
              <h4>{t('description')}</h4>
              <p>{property.description}</p>
            </div>
            <div className="property-news-single-card style-two border-bottom-yellow">
              <h4>{t('address')}</h4>
              {property.country &&
                <p><strong>{t('country')}</strong>: {property.country?.[i18n.language]}</p>}
              {property.city &&
                <p><strong>{t('city')}</strong>: {property.city?.[i18n.language]}</p>}
              {property.neighborhood &&
                <p><strong>{t('neighborhood')}</strong>: {property.neighborhood?.[i18n.language]}</p>}
              {property.address &&
                <p><strong>{t('address')}</strong>: {property.address}</p>}
            </div>
            <div className="property-news-single-card style-two border-bottom-yellow">
              <h4>{t('location')}</h4>
              <Maps draggable={false} initCoords={property.location} zoom={10} />
            </div>
            <div className="property-news-single-card border-bottom-yellow">
              <h4>{t('amenities')}</h4>
              <div className="row">
                {
                  Object.keys(property?.amenities ?? {})?.map(amenity => {
                    const amenitiesCol = Math.ceil(Object.keys(property?.amenities ?? {}).length / 3);

                    return (
                      <div className={`col-sm-${amenitiesCol}`} key={amenity}>
                        <ul className="rld-list-style mb-3 mb-sm-0">
                          <li><i className={"fa mb-1 fa-check"} /> {t(amenity)}</li>
                        </ul>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PropertyDetailsCard
