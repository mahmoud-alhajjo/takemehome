import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next';
import { useForm } from 'react-hook-form';
import { Button, Form } from 'react-bootstrap';
import classNames from 'classnames';
import { isEmpty } from 'ramda';
import { Field, useVerification } from '../../common';
import Btn from './Btn';

const VerificationCode = ({ 
  onSuccess = () => null,
  userInfo = {}
}) => {
  const { t } = useTranslation();
  const [validated, setValidated] = useState(false);
  const { handleSubmit, control, errors } = useForm();
  const [{ loading, data }, verifyAccount] = useVerification();

  const onSubmit = values => {
    setValidated(isEmpty(errors));
    verifyAccount({ data: {
      confirmCode: values.code,
      email: values.email
    } });
  };

  useEffect(() => {
    if (data?.code === 200) onSuccess(data);
  }, [data, onSuccess]);

  return (
    <Form
      className={classNames("contact-form-wrap", "contact-form-bg")}
      onSubmit={handleSubmit(onSubmit)}
      validated={validated}
      noValidate
    >
      <h4 className="mb-3">{t('enter_verification_code')}</h4>
      <Field
        name="email"
        type="email"
        placeholder={t('email')}
        control={control}
        errors={errors}
        defaultValue={userInfo?.email}
        rules={{
          required: t('required'),
          pattern: {
            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            message: t('invalid_email')
          }
        }}
      />
      <Field
        name="code"
        placeholder={t('verification_code')}
        control={control}
        errors={errors}
        rules={{ required: t('required') }}
      />
      <div className="btn-wrap">
        <Btn
          variant="yellow"
          type="submit"
          loading={loading}
        >{t('send')}</Btn>
      </div>
    </Form>
  )
}

export default VerificationCode
