import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { propertyFormatter, imageFallback } from '../helpers'

const SinglePropertyOverlay = ({
  item: _item
}) => {
  const { t } = useTranslation();
  const item = propertyFormatter(_item, '/property', t);

  return (
    <div className="col-xl-6 col-lg-8">
      <div className="single-leading-feature">
        <div className="slf-overlay" />
        <div className="thumb">
          <img src={item.image} className="w-100" alt="property" onError={imageFallback} />
        </div>
        <div className="details">
          <h4 className="title readeal-top"><Link to={item.url}>{item.title}</Link></h4>
          <h5 className="price">{item.newerprice}</h5>
          <span>{item.description}</span>
        </div>
      </div>
    </div>
  )
}

export default SinglePropertyOverlay
