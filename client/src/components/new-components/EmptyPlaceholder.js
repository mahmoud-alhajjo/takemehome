import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom';

const EmptyPlaceholder = ({
  text,
  btn
}) => {
  const { t } = useTranslation();

  return (
    <div className="empty-placeholder text-center mx-auto">
      <h5 className="mb-0">{text || t('no_data')}</h5>
      {btn && 
        <Link className="btn btn-yellow mt-4" to={btn?.url ?? "/"}>{btn?.text ?? t('go_home')}</Link>}
    </div>
  )
}

export default EmptyPlaceholder
