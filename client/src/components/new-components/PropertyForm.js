import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Dropzone } from '.';
import { Maps } from '../../containers/maps';
import { showAuthModalAction } from '../../store';
import { Button, CloseButton } from 'react-bootstrap';
import { debounce, Field } from '../../common';
import withSelectOptions from './withSelectOptions';
import {
  priceSellMarks,
  priceRentMarks,
  priceRentMax,
  priceSellMax,
  spaceMarks,
  spaceMax,
  amenities
} from '../config';
import Slider from 'rc-slider';
import Select from 'react-select';
import SelectCreatable from 'react-select/creatable';
import Btn from './Btn';
import { imageConverter, imageFallback, isMobile, numberFormatter } from '../helpers';
import { useParams } from 'react-router-dom';
import ImageGallery from 'react-image-gallery';

const PropertyForm = ({
  onSubmit = () => { },
  onChange = () => { },
  submitLoading: loading = false,
  initialValues,
  countries = {},
  cities = {},
  neighborhoods = {},
  bedrooms = {},
  bathrooms = {}
}) => {
  const { t, i18n } = useTranslation();
  const { accessToken: isAuthenticated, userInfo } = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const { propertyId } = useParams();

  const [values, setValues] = useState(initialValues);

  const { control, handleSubmit, errors, watch, reset } = useForm();
  const city = watch('city')?.value;

  useEffect(() => {
    onChange(city);
  }, [city]);

  useEffect(() => {
    if (propertyId) {
      setValues(v => ({ ...v, ...(initialValues || {}) }));
      reset({
        ...(initialValues || {}),
        country: countries.options?.find(({ value }) => value === initialValues?.country), // { value: 1, label: t('syria') },
        city: cities.options?.find(({ value }) => value === (city || initialValues?.city)),
        neighborhood: neighborhoods.options?.find(({ value }) => value === initialValues?.neighborhood)
      })
    }
  }, [initialValues, neighborhoods, countries, cities, propertyId]);

  const imageGalleryProps = propertyId ? {
    renderItem: item => {
      return (
        <div className="img-container">
          {!item.original?.startsWith?.('blob:') &&
            <CloseButton className="py-1 px-2 bg-danger text-light" onClick={() => setValues(v => ({ ...v, deletedImages: (v.deletedImages || [])?.concat?.([item.original]), images: v.images?.filter?.(img => img !== item.original) }))} />}
          <img src={item.original} onError={imageFallback} />
        </div>
      )
    }
  } : {}

  return (
    <form onSubmit={handleSubmit(formValues => onSubmit({ ...values, ...formValues }))}>
      <div className="row justify-content-center">
        <div className="col-xl-9 col-lg-10">
          <div className="section-title text-center">
            <h3>{t('add_new_property')}</h3>
          </div>
          {!isMobile &&
            <div className="border-bottom mb-4">
              <div className="row">
                <div className="col-md-4">
                  <div className="single-intro style-two text-center">
                    <div className="thumb">1</div>
                    <div className="details">
                      <h4 className="title">{t('choose_listing')}</h4>
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="single-intro style-two text-center">
                    <div className="thumb">2</div>
                    <div className="details">
                      <h4 className="title">{t('add_information')}</h4>
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="single-intro style-two text-center">
                    <div className="thumb">3</div>
                    <div className="details">
                      <h4 className="title">{t('publish')}</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>}
          <div className="row">
            <div className="col-5">
              <div className="section-title mb-md-0">
                <h4 className="pt-lg-1 pt-2">{t('categories')}</h4>
              </div>
            </div>
            <div className="col-7 text-right add-property-btn-wrap">
              <Button
                variant={values?.category === "sell" ? "yellow" : "secondary"}
                className="mr-3"
                onClick={() => setValues(v => ({ ...v, category: 'sell' }))}
              >{t('sell')}</Button>
              <Button
                variant={values?.category === "rent" ? "yellow" : "secondary"}
                onClick={() => setValues(v => ({ ...v, category: 'rent' }))}
              >{t('rent')}</Button>
            </div>
          </div>
          <div className="row pd-top-100">
            <div className="col-md-4">
              <div className="section-title">
                <h4><img src={"/assets/img/icons/28.png"} alt="img" />{t('images')}</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-lg-12 mb-3">
                  <Dropzone
                    onDrop={images => setValues(v => ({ ...v, images }))}
                  />
                </div>
                <div className="col-lg-12 mb-3">
                  {values.images && values.images.length > 0 && (
                    <ImageGallery
                      items={values.images?.map?.(img => ({ original: imageConverter(img), thumbnail: imageConverter(img) })) || []}
                      showThumbnails={values.images?.length > 1}
                      showPlayButton={values.images?.length > 1}
                      isRTL={i18n.language === 'ar'}
                      {...imageGalleryProps}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-100">
            <div className="col-md-4">
              <div className="section-title">
                <h4><img src={"/assets/img/icons/28.png"} alt="img" />{t('house_description')}</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-lg-12 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="title"
                      placeholder={t('title')}
                      control={control}
                      rules={{ required: t('required') }}
                      errors={errors}
                    />
                  </div>
                </div>
                <div className="col-lg-12 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="description"
                      placeholder={t('description')}
                      as="textarea"
                      rows={5}
                      control={control}
                      rules={{ required: t('required') }}
                      errors={errors}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-100">
            <div className="col-md-4">
              <div className="section-title">
                <h4><img src={"/assets/img/icons/28.png"} alt="img" />{t('house_information')}</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-lg-6 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="bedrooms"
                      component={Select}
                      placeholder={t('bedrooms')}
                      options={bedrooms.options}
                      control={control}
                      rules={{ required: t('required') }}
                      errors={errors}
                    />
                  </div>
                </div>
                <div className="col-lg-6 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="bathrooms"
                      component={Select}
                      placeholder={t('bathrooms')}
                      options={bathrooms.options}
                      control={control}
                      rules={{ required: t('required') }}
                      errors={errors}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-80">
            <div className="col-md-4">
              <div className="section-title">
                <h4><img src={"/assets/img/icons/30.png"} alt="img" />{t('area')}</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-12 mb-3">
                  <div className="rld-single-select mb-3">
                    <Slider
                      min={0}
                      max={spaceMax}
                      defaultValue={values.space}
                      className="mb-3"
                      marks={!isMobile ? spaceMarks : {}}
                      onChange={debounce(space => setValues(v => ({ ...v, space })))}
                    />
                  </div>
                  <span className="d-inline-block mt-3">{values.space} {t('square_meter')}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-80">
            <div className="col-md-4">
              <div className="section-title">
                <h4><img src={"/assets/img/icons/29.png"} alt="img" />{t('address')}</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-lg-6 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="country"
                      component={Select}
                      placeholder={t('country')}
                      isLoading={countries.loading}
                      options={countries.options}
                      control={control}
                      rules={{ required: t('required') }}
                      errors={errors}
                    />
                  </div>
                </div>
                <div className="col-lg-6 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="city"
                      component={Select}
                      placeholder={t('city')}
                      isLoading={cities.loading}
                      options={cities.options}
                      control={control}
                      rules={{ required: t('required') }}
                      errors={errors}
                    />
                  </div>
                </div>
                <div className="col-lg-12 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="neighborhood"
                      component={Select}
                      menuPlacement="top"
                      placeholder={t('neighborhood')}
                      isLoading={neighborhoods.loading}
                      options={neighborhoods.options}
                      control={control}
                      rules={{ required: t('required') }}
                      errors={errors}
                    />
                  </div>
                </div>
                {/* <div className="col-12 mb-2">
                  <Field
                    name="address"
                    placeholder={t('address')}
                    as="textarea"
                    rows={5}
                    control={control}
                    errors={errors}
                  />
                </div> */}
                <div className="col-12 text-center mb-3">
                  <h5>{t('or')}</h5>
                  <h6>{t('location_msg')}</h6>
                </div>
                <div className="col-12">
                  <Maps
                    onChange={location => setValues(v => ({ ...v, location }))}
                    initCoords={{ lat: values.location?.lat, lng: values.location?.lng }}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-80">
            <div className="col-md-4">
              <div className="section-title">
                <h4><img src={"/assets/img/icons/30.png"} alt="img" />{t('price')}</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-12 mb-3">
                  <div className="rld-single-select mb-3">
                    <Slider
                      min={0}
                      max={values.category === 'sell' ? priceSellMax : priceRentMax}
                      defaultValue={values.price}
                      className="mb-3"
                      marks={!isMobile ? values.category === 'sell' ? priceSellMarks : priceRentMarks : {}}
                      onChange={debounce(price => setValues(v => ({ ...v, price })))}
                    />
                  </div>
                  <span className="d-inline-block mt-3">{numberFormatter(values.price)}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-80">
            <div className="col-md-4">
              <div className="section-title">
                <h4><img src={"/assets/img/icons/31.png"} alt="img" />{t('amenities')}</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                {/* {
                  amenities.map(amenity => {
                    const amenitiesCol = Math.ceil(amenities.length / 3);

                    return (
                      <div className={`col-sm-${amenitiesCol}`} key={amenity}>
                        <ul className="rld-list-style mb-3 mb-sm-0">
                          <li>
                            <i
                              className={classNames("fa mb-1 cursor-pointer", values.amenities?.includes(amenity) ? "fa-check" : "fa-close")}
                              onClick={() => setValues(v => ({
                                ...v,
                                amenities: v.amenities?.includes(amenity) ?
                                  v.amenities.filter(vAmenity => vAmenity !== amenity) :
                                  [...v.amenities, amenity]
                              }))}
                            /> {t(amenity)}</li>
                        </ul>
                      </div>
                    )
                  })
                } */}
                <div className="col-12">
                  <p>{t('choose_amenities_message')}</p>
                  <Field
                    name="amenities"
                    isClearable
                    isMulti
                    menuPlacement="top"
                    component={SelectCreatable}
                    control={control}
                    errors={errors}
                    rules={{ required: t('required') }}
                    options={amenities.map(amenity => ({ label: t(amenity), value: amenity }))}
                  />
                </div>
                <div className="col-12 mt-5">
                  {isAuthenticated ? (
                    <Btn
                      variant="yellow"
                      type="submit"
                      loading={loading}
                    >{propertyId ? t('edit_property') : t('add_property')}</Btn>
                  ) : (
                      <Btn
                        variant="yellow"
                        type="button"
                        onClick={() => dispatch(showAuthModalAction({ showAuthModal: true }))}
                      >{(userInfo && !isAuthenticated) ? t('verify_account') : t('login')}</Btn>
                    )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  )
}

export default withSelectOptions(PropertyForm)
