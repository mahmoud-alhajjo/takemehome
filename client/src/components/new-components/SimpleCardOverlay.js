import React from 'react'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import { imageFallback } from '../helpers'

const SimpleCardOverlay = ({
  item
}) => {
  return (
    <div className={classNames("rld-city-item", item.class)}>
      <div className={classNames("single-city", item.sectionclass)}>
        <div className="sc-overlay" />
        <div className="thumb">
          <img src={item.image} alt="city" onError={imageFallback} />
        </div>
        <div className="details">
          <h5 className="title"><Link to={item.url}>{item.title}</Link></h5>
          <p>{item.content}</p>
        </div>
      </div>
    </div>
  )
}

export default SimpleCardOverlay
