import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import Login from './Login';
import Register from './Register';
import { Tabs, Tab } from 'react-bootstrap';
import SweetAlert from './SweetAlert';
import { useDispatch, useSelector } from 'react-redux';
import VerificationCode from './VerificationCode';
import { showAuthModalAction } from '../../store';

const AuthModal = ({
  show = false,
  onSuccess = () => null
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { userInfo } = useSelector(state => state.auth);

  const handleSuccess = (data) => {
    SweetAlert.close();
    onSuccess(data);
  }

  useEffect(() => {
    if (show) {
      SweetAlert.fire({
        html: !userInfo ? (
          <Tabs defaultActiveKey="login" id="auth-tabs" unmountOnExit>
            <Tab eventKey="login" title={t('login')}>
              <Login onSuccess={handleSuccess} />
            </Tab>
            <Tab eventKey="register" title={t('register')}>
              <Register onSuccess={handleSuccess} />
            </Tab>
            <Tab eventKey="verification" title={t('verification_code')}>
              <VerificationCode userInfo={userInfo} onSuccess={handleSuccess} />
            </Tab>
          </Tabs>
        ) : (
          <VerificationCode userInfo={userInfo} onSuccess={handleSuccess} />
        ),
        showConfirmButton: false,
        showClass: { popup: 'auth-swal' }
      }).then(result => {
        if (result.isDismissed) {
          dispatch(showAuthModalAction({ showAuthModal: false }));
        }
      });
    }
  }, [show]);

  return (
    <div></div>
  )
}

export default AuthModal
