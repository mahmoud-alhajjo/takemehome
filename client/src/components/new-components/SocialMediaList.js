import React from 'react'
import { socialMediaLinks } from '../config'

const SocialMediaList = () => {
  return (
    <ul className="social-icon">
      <li className="ml-0">
        <a href={socialMediaLinks.facebook} rel="noopener noreferrer" target="_blank"><i className="fa fa-facebook" /></a>
      </li>
      <li>
        <a href={socialMediaLinks.twitter} rel="noopener noreferrer" target="_blank"><i className="fa fa-twitter" /></a>
      </li>
      <li>
        <a href={socialMediaLinks.linkedin} rel="noopener noreferrer" target="_blank"><i className="fa fa-linkedin" /></a>
      </li>
    </ul>
  )
}

export default SocialMediaList
