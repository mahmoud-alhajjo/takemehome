import React, { Fragment, useEffect, useState } from 'react'
import { Pagination } from 'react-bootstrap'
import { useHistory, useLocation } from 'react-router-dom'
import qs from 'qs'

const withPagination = Component => ({
  refetch = () => { },
  currentPage = 0,
  lastPage,
  ...rest
}) => {
  // const history = useHistory();
  // const location = useLocation();
  const [page, setPage] = useState();

  useEffect(() => {
    if (page || page === 0) {
      // history.push(`${location.pathname}?page=${page}`);
      refetch(page);
    }
  }, [page]);

  // useEffect(() => {
  //   const qsParams = qs.parse(location.search?.substring?.(1) ?? '') || {};

  //   setPage(parseInt?.(qsParams.page) || 1);
  // }, [location])

  return (
    <Component
      lastPage={lastPage}
      currentPage={currentPage}
      onPageChange={setPage}
      {...rest}
    />
  )
}

const Pager = ({
  lastPage = 0,
  firstPage = 0,
  currentPage: activePage = 0,
  show = 5,
  onPageChange = () => { },
  children
}) => {
  const [currentPage, setCurrentPage] = useState(activePage);

  useEffect(() => {
    setCurrentPage(activePage)
  }, [activePage])

  const handleChange = (page) => {
    setCurrentPage(page);
    onPageChange(page);
  }
  
  return (
    <Fragment>
      {children}
      {lastPage > 1 &&
        <Pagination className="justify-content-center">
          <Pagination.First
            disabled={currentPage === firstPage}
            onClick={() => handleChange(firstPage)}
          />
          <Pagination.Prev
            disabled={currentPage === firstPage}
            onClick={() => handleChange(currentPage - 1)}
          />
          {activePage > show && <Pagination.Ellipsis disabled />}
          {
            Array.from(Array(lastPage).keys())
              .slice(
                (activePage - show) > 0 ? activePage - show : 0,
                (activePage + show) < lastPage ? activePage + show : lastPage
              ).map(page => {
                page += 1;
                const isActive = activePage === page;
                return (
                  <Pagination.Item
                    key={page}
                    active={isActive}
                    onClick={() => handleChange(page)}
                  >{page}</Pagination.Item>
                )
              })
          }

          {(lastPage - activePage) > show && <Pagination.Ellipsis disabled />}
          <Pagination.Next
            disabled={currentPage === lastPage}
            onClick={() => handleChange(currentPage + 1)}
          />
          <Pagination.Last
            disabled={currentPage === lastPage}
            onClick={() => handleChange(lastPage)}
          />
        </Pagination>}
    </Fragment>
  )
}

export default withPagination(Pager)
