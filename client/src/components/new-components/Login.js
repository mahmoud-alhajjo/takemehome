import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next';
import { useForm } from 'react-hook-form';
import { Button, Form } from 'react-bootstrap';
import classNames from 'classnames';
import { isEmpty } from 'ramda';
import { useLogin, Field } from '../../common';
import Btn from './Btn';

const Login = ({ 
  onSuccess = () => null 
}) => {
  const { t } = useTranslation();
  const [validated, setValidated] = useState(false);
  const { handleSubmit, control, errors } = useForm();
  const [{ loading, data }, loginUser] = useLogin();

  const onSubmit = values => {
    setValidated(isEmpty(errors));
    loginUser({ data: values });
  };

  useEffect(() => {
    if (data?.code === 200) onSuccess(data);
  }, [data, onSuccess]);

  return (
    <Form
      className={classNames("contact-form-wrap", "contact-form-bg")}
      onSubmit={handleSubmit(onSubmit)}
      validated={validated}
      noValidate
    >
      <h4>{t('login')}</h4>
      <Field
        name="email"
        type="email"
        placeholder={t('email')}
        control={control}
        errors={errors}
        rules={{
          required: t('required'),
          pattern: {
            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            message: t('invalid_email')
          }
        }}
      />
      <Field
        name="password"
        type="password"
        placeholder={t('password')}
        control={control}
        errors={errors}
        rules={{ required: t('required') }}
      />
      <div className="btn-wrap">
        <Btn
          variant="yellow"
          type="submit"
          loading={loading}
        >{t('login')}</Btn>
      </div>
    </Form>
  )
}

export default Login
