import React from 'react'
import { Link } from 'react-router-dom'
import { articleFormatter, imageFallback } from '../helpers';

const SingleArticleHorizontal = ({
  item: _item
}) => {
  const item = articleFormatter(_item);

  return (
    <div className="item">
      <Link to={item.url} className="media single-popular-post">
        <div className="media-left">
          <img src={item.image} alt="article" width={60} height={60} onError={imageFallback} />
        </div>
        <div className="media-body">
          <h6>{item.title}</h6>
          <span>{item.date}</span>
        </div>
      </Link>
    </div>
  )
}

export default SingleArticleHorizontal
