import React from 'react'
import { Button, Spinner } from 'react-bootstrap'

const Loader = (props) => {
  return (
    <Spinner
      animation="border"
      variant="light"
      size="md"
      className="my-auto d-block"
      {...props}
    />
  )
}

const Btn = ({
  children,
  loading = false,
  loaderProps = {},
  ...props
}) => {
  return (
    <Button
      disabled={loading}
      {...props}
    >{loading ? <Loader {...loaderProps} /> : children}</Button>
  )
}

export default Btn
