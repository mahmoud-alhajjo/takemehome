import React from 'react'
import { Dropdown, NavItem, NavLink } from 'react-bootstrap'
import { userImagePlaceholder } from '../helpers'

const ProfileHeader = ({
  menu = [],
  userInfo = {}
}) => {
  return (
    <div className="profile-header" style={styles.container}>
      <Dropdown as={NavItem}>
        <Dropdown.Toggle as={NavLink} style={styles.toggler}>
          <img src={userImagePlaceholder} alt="user" style={styles.image} />
          <strong style={styles.title}>{userInfo?.name}</strong>
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {menu.map(item => (
            <Dropdown.Item key={item.key} href={`#${item.key}`} onClick={item.onClick} style={styles.item} {...item}>{item.label}</Dropdown.Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>
    </div>
  )
}

const styles = {
  container: {
    display: 'flex'
  },
  image: {
    width: 30,
    height: 30,
    marginRight: 10,
    borderRadius: 100
  },
  toggler: {
    display: 'flex',
    alignItems: 'center'
  },
  item: {
    fontSize: 14
  }
}

export default ProfileHeader
