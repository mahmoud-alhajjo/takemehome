import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next';
import { useForm } from 'react-hook-form';
import { Button, Form } from 'react-bootstrap';
import classNames from 'classnames';
import { isEmpty } from 'ramda';
import { useRegister, Field } from '../../common';
import Btn from './Btn';

const Register = ({
  onSuccess = () => null
}) => {
  const { t } = useTranslation();
  const [validated, setValidated] = useState(false);
  const { handleSubmit, control, errors } = useForm();
  const [{ loading, data }, registerUser] = useRegister();

  const onSubmit = values => {
    setValidated(isEmpty(errors));
    registerUser({
      data: {
        ...values,
        type: "client"
      }
    });
  };

  useEffect(() => {
    if (data?.code === 200) onSuccess(data);
  }, [data, onSuccess]);

  return (
    <Form
      className={classNames("contact-form-wrap", "contact-form-bg")}
      onSubmit={handleSubmit(onSubmit)}
      validated={validated}
      noValidate
    >
      <h4>{t('register')}</h4>
      <Field
        name="name"
        type="text"
        placeholder={t('name')}
        control={control}
        rules={{ required: t('required') }}
        errors={errors}
      />
      <Field
        name="phone"
        type="number"
        placeholder={t('phone')}
        control={control}
        rules={{ required: t('required') }}
        errors={errors}
      />
      <Field
        name="email"
        type="email"
        placeholder={t('email')}
        control={control}
        errors={errors}
        rules={{
          required: "Required",
          pattern: {
            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            message: t('invalid_email')
          }
        }}
      />
      <Field
        name="password"
        type="password"
        placeholder={t('password')}
        control={control}
        errors={errors}
        rules={{ required: "Required" }}
      />
      <div className="btn-wrap">
        <Btn
          variant="yellow"
          type="submit"
          loading={loading}
        >{t('register')}</Btn>
      </div>
    </Form>
  )
}

export default Register
