import React, {useCallback, useState} from 'react'
import {useDropzone} from 'react-dropzone'
import { useTranslation } from 'react-i18next';

const Dropzone = ({
  onDrop: handleDrop = function() {}
}) => {
  const { t } = useTranslation();
  const [files, setFiles] = useState();
  const onDrop = useCallback(acceptedFiles => {
    console.log(acceptedFiles);
    setFiles(acceptedFiles);
    handleDrop(acceptedFiles);
  }, [handleDrop]);

  const { 
    getRootProps, 
    getInputProps, 
    isDragActive
  } = useDropzone({ onDrop, accept: ['image/png', 'image/jpg', 'image/jpeg'] });

  return (
    <div className="dropzone" {...getRootProps()}>
      <input {...getInputProps()} />
      {
        files ? 
          files.map(file => <p key={JSON.stringify(file)}>{file.name}</p>)
        : isDragActive ?
          <p>{t('drop_images_here')}</p> :
          <p>{t('drag_or_click_to_select')}</p>
      }
    </div>
  )
}

export default Dropzone;
