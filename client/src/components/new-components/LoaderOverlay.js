import React from 'react'
import { Spinner } from 'react-bootstrap'
import { useTranslation } from 'react-i18next'

const LoaderOverlay = () => {
  const { t } = useTranslation();
  
  return (
    <div style={styles.container}>
      <Spinner animation="border" variant="light" size="xl" />
      <h4 style={styles.text}>{t('fetching_data')}</h4>
    </div>
  )
}

const styles = {
  container: { 
    display: 'flex', 
    flexDirection: 'column', 
    justifyContent: 'center', 
    alignItems: 'center', 
    position: "fixed", 
    bottom: 0, 
    top: 0, 
    left: 0, 
    right: 0, 
    zIndex: '999999999999999', 
    backgroundColor: 'rgba(0,0,0,.8)' 
  },
  text: { 
    color: 'white', 
    marginTop: '1rem' 
  }
}

export default LoaderOverlay
