import SweetAlert from './SweetAlert';
import { i18n, API } from '../../common';
import cogoToast from 'cogo-toast';

const deleteModal = (url, t = i18n.t) => {
  return SweetAlert.fire({
    title: t('delete_confirm_message'),
    showCancelButton: true,
    confirmButtonText: t('delete'),
    cancelButtonText: t('cancel'),
    showLoaderOnConfirm: true,
    preConfirm: () => {
      return API.delete(url)
        .then(response => {
          if (response.status >= 300) {
            throw new Error(response.statusText)
          }
          return response
        })
        .catch(error => {
          SweetAlert.showValidationMessage(
            `Request failed: ${error}`
          )
        })
    },
    allowOutsideClick: () => !SweetAlert.isLoading()
  }).then((result) => {
    if (result?.value?.data?.code < 300) {
      SweetAlert.close();
      cogoToast.success(t('success_delete_message'));
      return Promise.resolve(result);
    }
  })
}

export default deleteModal
