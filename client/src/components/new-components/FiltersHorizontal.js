import React, { useState } from 'react';
import { useDidMountEffect } from '../hooks';
import { useTranslation } from 'react-i18next';
import { useNeighborhoods, useCities } from '../../common';
import { Button, Tabs, Tab } from 'react-bootstrap';
import Select from 'react-select';
import { priceSellMax, priceRentMax, formatOptionsWithLang } from '../config';
import Slider from 'rc-slider';
import qs from 'qs';
import { useHistory } from 'react-router-dom';
import { numberFormatter } from '../helpers';

const FiltersHorizontal = ({
  onChange = () => {}
}) => {
  const { t, i18n } = useTranslation();
  const history = useHistory();

  const [category, setCategory] = useState('sell');
  const [title, setTitle] = useState(undefined);
  const [city, setCity] = useState(undefined);
  const [neighborhood, setNeighborhood] = useState(undefined);
  const [price, setPrice] = useState(priceSellMax);

  const { data: cities, loading: loadingCities } = useCities();
  const citiesOptions = formatOptionsWithLang(cities, i18n.language);

  const { data: neighborhoods, loading: loadingNeighborhoods } = useNeighborhoods({ perPage: 10000, filters: city ? { cityId: city } : undefined });
  const neighborhoodsOptions = formatOptionsWithLang(neighborhoods, i18n.language);

  useDidMountEffect(() => {
    onChange({
      category,
      title,
      city,
      neighborhood,
      price
    })
  }, [category, title, city, neighborhood, price])

  const handleSubmit = (e) => {
    e.preventDefault();
    history.push(`/search?${qs.stringify({ category, title, city, neighborhood, price })}`);
  }

  const filters = (
    <form onSubmit={handleSubmit} className="filters-bar--content">
      <div className="rld-single-input left-icon">
        <input
          type="text"
          placeholder={t('search_home')}
          value={title}
          onChange={({ target: { value = "" } = {} }) => setTitle(value)}
        />
      </div>
      <div className="rld-single-select">
        <Select
          placeholder={t('city')}
          options={citiesOptions}
          isLoading={loadingCities}
          onChange={({ value }) => setCity(value)}
        />
      </div>
      <div className="rld-single-select">
        <Select
          placeholder={t('neighborhood')}
          options={neighborhoodsOptions}
          isLoading={loadingNeighborhoods}
          onChange={({ value }) => setNeighborhood(value)}
        />
      </div>
      <div className="rld-price-slider-wrap">
        <div className="price d-flex justify-content-between">
          <span>{t('price')}</span>
          <span className="float-right">{numberFormatter(price)} {t('sp')}</span>
        </div>
        <Slider
          min={0}
          max={category === 'sell' ? priceSellMax : priceRentMax}
          onChange={setPrice}
        />
      </div>
      <div className="rld-price-slider-wrap d-flex justify-content-center">
        <Button
          variant="yellow"
          className="float-right"
          type="submit"
        >{t('search')}</Button>
      </div>
    </form>
  )

  return (
    <div className="filters-bar horizontal w-100">
      <Tabs defaultActiveKey="sell" activeKey={category} onSelect={setCategory} id="filters-bar-horizontal">
        <Tab eventKey="sell" title={t('sell')}>
          {filters}
        </Tab>
        <Tab eventKey="rent" title={t('rent')}>
          {filters}
        </Tab>
      </Tabs>
    </div>
  )
}

export default React.memo(FiltersHorizontal)
