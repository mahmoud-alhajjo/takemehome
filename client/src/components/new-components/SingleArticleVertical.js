import React from 'react'
import { Link } from 'react-router-dom'
import { articleFormatter, imageFallback } from '../helpers';

const SingleArticleVertical = ({
  item: _item
}) => {
  const item = articleFormatter(_item);

  return (
    <div className="col-lg-6">
      <div className="single-news">
        <div className="thumb">
          <img src={item.image} alt="blog" />
        </div>
        <div className="details">
          <h4><Link to={item.url}  >{item.title}</Link></h4>
          <p>{item.description}</p>
          <div className="author">
            <img src={item.authorimage} alt="blog author" width={40} height={40} onError={imageFallback} />
            <span>{item.author}</span>
            <span className="date">{item.date}</span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SingleArticleVertical
