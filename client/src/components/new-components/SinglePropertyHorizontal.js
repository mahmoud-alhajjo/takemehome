import React from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { propertyFormatter, imageFallback } from '../helpers';
import deleteModal from './DeleteModal';

const SinglePropertyHorizontal = ({
  item: _item,
  detailsUrlPrefix,
  withActions = false,
  onDelete = () => {}
}) => {
  const { t, i18n } = useTranslation();
  const item = propertyFormatter(_item, detailsUrlPrefix, t);

  const handleDelete = event => {
    event.preventDefault();
    deleteModal(`admin/properties/${item.id}`, t).then(onDelete);
  }
  
  return (
    <div className="single-feature style-two">
      <div className="thumb">
        <img src={item.image} alt="img" onError={imageFallback} />
      </div>
      <span className="category">{t(item.category)}</span>
      <div className="details">
        <div className="details-wrap">
          {/* <div className="feature-logo">
            <img src={item.icon} alt="feature" className="rounded" />
          </div> */}
          <p className="author"><i className="fa fa-user" /> {item.authorname}</p>
          <h6 className="title readeal-top"><Link to={item.url}>{item.title}</Link></h6>
          <p className="author"><i className="fa fa-map-marker" /> {item.city?.[i18n.language]} - {item.neighborhood?.[i18n.language]}</p>
          <p className="author">{item.description}</p>
          <h6 className="price">{item.newerprice}</h6>
          <ul className="info-list">
            {item.features.map((features, i) =>
              <li key={i} ><i className={features.icon} /> {features.title}</li>
            )}
            <li><img src={"/assets/img/icons/7.png"} alt="feature" /> {item.area}  </li>
          </ul>
          <ul className="contact-list">
            <li className="readeal-top"><Link className="btn btn-yellow" to={item.url}>{t('view_details')}</Link></li>
            {withActions && 
              <>
                <li className="readeal-top"><Link className="message" to={`/edit-property/${item.id}`} ><i className="fa fa-edit" /></Link></li>
                <li className="readeal-top"><a className="phone bg-danger" href="#delete" onClick={handleDelete}><i className="fa fa-trash text-white" /></a></li>
              </>}
          </ul>
        </div>
      </div>
    </div>
  )
}

export default SinglePropertyHorizontal
