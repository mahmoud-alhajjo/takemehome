import React, { useState } from 'react'
import { useDidMountEffect } from '../hooks';
import { useTranslation } from 'react-i18next';
import { useNeighborhoods, useCities } from '../../common';
import { Button } from 'react-bootstrap';
import Select from 'react-select';
import { priceSellMax, priceRentMax, spaceMax, bedroomsOptions, bathroomsOptions, formatOptionsWithLang } from '../config';
import Slider, { Range } from 'rc-slider';
import { useHistory } from 'react-router-dom';
import qs from 'qs';
import { isMobile, numberFormatter, removeEmpty } from '../helpers';

const Filters = ({
  initialFilters = {
    category: undefined,
    title: undefined,
    city: undefined,
    neighborhood: undefined,
    price: undefined,
    space: undefined,
    bedrooms: undefined,
    bathrooms: undefined
  },
  onChange = console.log
}) => {
  const { t, i18n } = useTranslation();
  const history = useHistory();

  const [category, setCategory] = useState(initialFilters.category);
  const [title, setTitle] = useState(initialFilters.title);
  const [city, setCity] = useState(parseInt(initialFilters.city));
  const [neighborhood, setNeighborhood] = useState(parseInt(initialFilters.neighborhood));
  const [price, setPrice] = useState(parseInt(initialFilters.price) || priceSellMax);
  const [space, setSpace] = useState(parseInt(initialFilters.space) || spaceMax);
  const [bedrooms, setBedrooms] = useState(parseInt(initialFilters.bedrooms));
  const [bathrooms, setBathrooms] = useState(parseInt(initialFilters.bathrooms));

  const { data: cities, loading: loadingCities } = useCities();
  const citiesOptions = formatOptionsWithLang(cities, i18n.language);

  const { data: neighborhoods, loading: loadingNeighborhoods } = useNeighborhoods({
    perPage: 10000,
    filters: city ? { cityId: city } : undefined
  });
  const neighborhoodsOptions = formatOptionsWithLang(neighborhoods, i18n.language);

  useDidMountEffect(() => {
    const filters = removeEmpty({
      category,
      title,
      city,
      neighborhood,
      price,
      space,
      bedrooms,
      bathrooms
    })
    onChange(filters);
    history.push(`/search?${qs.stringify(filters)}`);
  }, [category, title, city, neighborhood, price, space, bedrooms, bathrooms]);

  useDidMountEffect(() => {
    setNeighborhood(undefined);
  }, [city]);

  const handleReset = () => {
    setCategory(undefined);
    setTitle(undefined);
    setCity(undefined);
    setNeighborhood(undefined);
    setPrice(priceSellMax);
    setSpace(spaceMax);
    setBedrooms(undefined);
    setBathrooms(undefined);
    history.push('/search');
  }

  return (
    <div className="filters-bar">
      <h6 className="filter-title mb-4">
        <img className="mr-3" src={"/assets/img/icons/18.png"} alt="filter" />
        {t('filters')}
      </h6>
      <form className="widget widget-sidebar-search-wrap d-flex">
        <div className="widget-sidebar-search">
          <div className="title">{t('category')}</div>
          <div className="widget-sidebar-item-wrap btn-area d-flex justify-content-between">
            <Button
              variant={category === 'sell' ? "yellow" : "secondary"}
              onClick={() => setCategory('sell')}
            >{t('for_sell')}</Button>
            <Button
              variant={category === 'rent' ? "yellow" : "secondary"}
              className="float-right"
              onClick={() => setCategory('rent')}
            >{t('for_rent')}</Button>
          </div>
          <div className="widget-sidebar-item-wrap rld-single-input left-icon">
            <input
              type="text"
              placeholder={t('search_home')}
              value={title}
              onChange={({ target: { value = "" } = {} }) => setTitle(value)}
            />
          </div>
          <div className="widget-sidebar-item-wrap rld-single-select">
            <Select
              placeholder={t('city')}
              options={citiesOptions}
              value={citiesOptions?.filter(option => option.value === city)}
              isLoading={loadingCities}
              onChange={({ value }) => setCity(value)}
            />
          </div>
          <div className="widget-sidebar-item-wrap rld-single-select">
            <Select
              placeholder={t('neighborhood')}
              options={neighborhoodsOptions}
              value={neighborhoodsOptions?.filter(option => option.value === neighborhood)}
              isLoading={loadingNeighborhoods}
              onChange={({ value }) => setNeighborhood(value)}
            />
          </div>
          <div className="widget-sidebar-item-wrap rld-price-slider-wrap">
            <div className="title">{t('price')}</div>
            <div className="price mb-3">
              <span className="float-right">{category === 'sell' ? (numberFormatter(price) || priceSellMax) : (numberFormatter(price) || priceRentMax)} {t('sp')}</span>
            </div>
            <Slider
              min={0}
              max={category === 'sell' ? priceSellMax : priceRentMax}
              value={price}
              onChange={setPrice}
            />
          </div>
          <div className="widget-sidebar-item-wrap rld-price-slider-wrap">
            <div className="title">{t('area')}</div>
            <div className="price mb-3">
              <span className="float-right">{parseInt(space)} {t('sq_meter')}</span>
            </div>
            <Slider
              min={0}
              max={spaceMax}
              onChange={setSpace}
              value={space}
            />
          </div>
          <div className="widget-sidebar-item-wrap rld-single-select-wrap">
            <div className="title pt-2">{t('bedrooms')}</div>
            <div className="rld-single-select">
              <Range
                min={0}
                max={10}
                marks={{0:0,1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9,10:10}}
                onChange={setBedrooms}
                value={bedrooms instanceof Array ? bedrooms : [0, 10]}
              />
              {/* <Select
                placeholder={t('bedrooms')}
                options={bedroomsOptions(t)}
                onChange={({ value }) => setBedrooms(value)}
              /> */}
            </div>
          </div>
          <div className="widget-sidebar-item-wrap rld-single-select-wrap">
            <div className="title pt-2">{t('bathrooms')}</div>
            <div className="rld-single-select">
              <Range
                min={0}
                max={10}
                marks={{0:0,1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9,10:10}}
                onChange={setBathrooms}
                value={bathrooms instanceof Array ? bathrooms : [0, 10]}
              />
              {/* <Select
                placeholder={t('bathrooms')}
                options={bathroomsOptions(t)}
                onChange={({ value }) => setBathrooms(value)}
              /> */}
            </div>
          </div>
          <div className="widget-sidebar-item-wrap rld-single-select-wrap">
            <Button
              variant="yellow"
              onClick={() => handleReset()}
              block
              className="mt-3"
            >{t('reset')}</Button>
          </div>
        </div>
        {isMobile &&
          <Button
            variant="yellow"
            onClick={() => document.querySelector('.properties-area').scrollIntoView()}
            style={{ position: 'fixed', bottom: 54, left: 0, right: 0, display: 'block', width: '100%', zIndex: 99 }}
          >{t('search')}</Button>}
      </form>
    </div>
  )
}

export default Filters
