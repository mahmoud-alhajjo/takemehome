import React from 'react';
import { useParams } from 'react-router-dom';
import { useArticles } from '../../common';
import { articleFormatter } from '../helpers';
import DisqusComments from './DisqusComments';

const BlogDetails = () => {
  const { articleId } = useParams();
  const [{ data: { content: { data: _data } = {} } = {} }] = useArticles({
    id: articleId
  });

  const data = articleFormatter(_data ?? {});

  return (
    <div>
      <div className="news-details-area">
        <div className="container">
          <div className="news-details-author-social">
            <div className="row">
              <div className="col-sm-6 mb-4 mg-sm-0">
                <div className="author">
                  <img src={"/assets/img/user-placeholder.png"} alt="news" />
                  <p>{data.author}</p>
                  <p>{data.date}</p>
                </div>
              </div>
            </div>
          </div>
          <div className="row justify-content-center pd-top-70">
            <div className="col-lg-8">
              <div className="news-details-wrap">
                <h3 className="title1">{data.title}</h3>
                <img className="news-details-thumb" src={data.image} alt="img" />
                <p dangerouslySetInnerHTML={{ __html: data.content }} />
              </div>
              <DisqusComments />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default BlogDetails;
