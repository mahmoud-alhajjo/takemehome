import React from 'react';
import { ArticlesSection } from '../../containers';

const PostList = ({ search }) => {
  return (
    <div className="property-news-area pd-top-100 pd-bottom-70">
      <div className="container">
        <div className="row">
          <ArticlesSection
            filters={{ search: search || undefined }}
          />
        </div>
      </div>
    </div>
  )
}

export default PostList;
