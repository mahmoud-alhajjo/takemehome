import React, { useEffect } from 'react'

const DisqusComments = () => {

  useEffect(() => {
    appendDisqusScript();
  }, []);

  const appendDisqusScript = () => {
    var d = document, s = d.createElement('script');
    s.src = 'https://takemehome-2.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
  }

  return (
    <div>
      <div id="disqus_thread"></div>
    </div>
  )
}

export default DisqusComments
