import React from 'react';
import { useTranslation } from 'react-i18next';

const PopularPost = ({ onChange = () => {} }) => {
  const { t } = useTranslation();

  return (
    <div>
      <div className="popular-post-area">
        <div className="container">
          <div className="post-and-search">
            <div className="news-search-btn">
              <i className="fa fa-search" />
            </div>
            <form className="news-search-box" onSubmit={e => e.preventDefault()}>
              <input 
                type="text" 
                placeholder={t('search')}
                onChange={({ target: { value } = {} }) => onChange(value)}
              />
              <button><i className="fa fa-search" /></button>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PopularPost;
