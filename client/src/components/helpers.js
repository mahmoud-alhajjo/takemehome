import { LocalStorage } from "../common";
import MobileDetect from 'mobile-detect';
import { bedroomsOptions, bathroomsOptions } from "./config";

export const addNewPropertyFormatter = data => {

  const objAmenities = data.amenities?.reduce(function (acc, amenity, idx) {
    acc = {
      ...acc,
      [amenity.value]: true
    }
    return acc;
  }, {})

  return toFormData({
    _method: data._method || undefined,
    deletedImages: data.deletedImages || undefined,
    images: data.images,
    name: data.title,
    description: data.description,
    countryId: data.country?.value ?? 1,
    cityId: data.city?.value,
    neighbourhoodId: data.neighborhood?.value,
    owner: 'none',
    seller: 'none',
    bathroomNumber: data.bathrooms?.value || 0,
    bedroomNumber: data.bedrooms?.value || 0,
    space: data.space,
    price: data.price,
    contractType: data.category,
    rating: 5,
    views: 1,
    builtAt: '2020-12-12',
    location: data.location,
    amenities: JSON.stringify(objAmenities),
    additionals: '[]',
    constructionStatus: 'good'
  })
}

export const addArticleFormatter = data => {

  const objAmenities = data.amenities?.reduce(function (acc, amenity, idx) {
    acc = {
      ...acc,
      [amenity]: true
    }
    return acc;
  }, {})

  return toFormData({
    images: data.images,
    name: data.title,
    description: data.description,
    countryId: 1, // data.country?.value,
    cityId: data.city?.value,
    neighbourhoodId: data.neighborhood?.value,
    owner: 'none',
    seller: 'none',
    bathroomNumber: data.bathrooms?.value,
    bedroomNumber: data.bedrooms?.value,
    space: data.space,
    price: data.price,
    contractType: data.category,
    rating: 5,
    views: 1,
    builtAt: '2020-12-12',
    location: '33.5138.36.2765',
    amenities: JSON.stringify(objAmenities),
    additionals: '[]',
    constructionStatus: 'good',
    status: data.status
  })
}

// additionals: { blabla: "ddddssssss", dddd: "sdsdf" }
// amenities: { airConditioning: true, lawn: true, dryer: true, microwave: true, souna: true, windowCovering: true, … }
// bathroomNumber: 6
// bedroomNumber: 0
// builtAt: "1994-08-08"
// cityId: 1
// cityName: "Damascus"
// constructionStatus: "super deluxe"
// contractType: "buying"
// countryId: 1
// countryName: "syria"
// createdAt: "2020-08-28T19:57:25.000000Z"
// description: "Odit est quasi laudantium sunt et."
// id: 2
// images: ["images/dcsdcsdc/deNCJl6EXCSY3rk3WAtkvFJCJ48NXLlb2PsLQmvU.jpeg", …]
// location: "726567af4ebd9034c299f8a8a247328e37188822"
// name: "Bungalow"
// neighbourhoodId: 1
// neighbourhoodName: "Al Hurriya_ Ish Al Werwer"
// owner: "Gayle Schaden"
// price: "3076.270945871"
// rating: 3
// seller: "Gayle Schaden"
// space: "39264282.89535"
// status: "accepted"
// updatedAt: "2020-08-28T12:13:57.000000Z"
// userId: 4
// userName: "Brandon Considine II"
// views: 4087

export const imagePrefix = 'https://f002.backblazeb2.com/file/take-me-home/';
export const propertyImagePlaceholder = "/assets/img/property-placeholder.png";
export const userImagePlaceholder = "/assets/img/user-placeholder.png";

export const withImagePrefix = (imgUrl) => {
  return imgUrl ? imagePrefix + imgUrl : null;
}

export const removeImagePrefix = (imgWithPrefix) => {
  const imgWithoutPrefix = imgWithPrefix.split(imagePrefix)[1];
  return imgWithoutPrefix;
}

export const citiesLocations = {
  damascus: {
    lat: 33.5116709,
    lng: 36.2711847
  }
}

export const propertyFormatter = (data, detailsUrlPrefix = '/property', t = v => v) => {
  const images = data.images?.map?.(img => img ? withImagePrefix(img) : propertyImagePlaceholder);
  return {
    id: data.id,
    title: data.name,
    category: data.contractType,
    image: images?.length ? images[0] : propertyImagePlaceholder,
    images,
    icon: userImagePlaceholder,
    url: `${detailsUrlPrefix}/${data.id}`,
    olderprice: data.contractType === 'sell' ? numberFormatter(parseFloat(data.price)) : `${numberFormatter(parseFloat(data.price))} ${t('sp_per_mo')}`,
    newerprice: data.contractType === 'sell' ? numberFormatter(parseFloat(data.price)) : `${numberFormatter(parseFloat(data.price))} ${t('sp_per_mo')}`,
    area: `${parseInt(data.space)} ${t('sq')}`,
    authorname: data.userName,
    phone: data.phone,
    email: data.email,
    cat: `${data.contractType}`,
    features: [
      {
        "icon": "fa fa-bed",
        "title": `${data.bedroomNumber} ${t('bed')}`
      },
      {
        "icon": "fa fa-bath",
        "title": `${data.bathroomNumber} ${t('bath')}`
      },
      {
        "icon": "fa fa-eye",
        "title": `${data.views} ${t('view')}`
      }
    ],
    description: data.description,
    amenities: data.amenities,
    views: data.views,
    rate: data.rating,
    status: data.status,
    location: data.location || {},
    country: data.countryName,
    city: data.cityName,
    neighborhood: data.neighbourhoodName,
    address: data.address
  }
}

export const articleFormatter = data => {
  return {
    id: data.id,
    title: data.name,
    url: `/blog/${data.id}`,
    image: data.image ? withImagePrefix(data.image) : "/assets/img/news/2.jpg",
    author: "Admin",
    authorimage: "/assets/img/user-placeholder.png",
    date: new Date(data.createdAt).toLocaleDateString(),
    content: data.content,
    description: data.description
  }
}

export function toFormData(obj, form, namespace) {
  let fd = form || new FormData();
  let formKey;

  for (let property in obj) {
    if (obj.hasOwnProperty(property) && (obj[property] || obj[property] === 0)) {
      if (namespace) {
        formKey = namespace + '[' + property + ']';
      } else {
        formKey = property;
      }

      // if the property is an object, but not a File, use recursivity.
      if (obj[property] instanceof Date) {
        fd.append(formKey, obj[property].toISOString());
      }
      else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
        toFormData(obj[property], fd, formKey);
      } else { // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }
    }
  }

  return fd;
}

export const removeEmpty = obj => {
  Object.keys(obj).forEach(key => !obj[key] && delete obj[key]);
  return obj;
};

// export const toQueryStringObject = (obj, namespace) => {
//   if (namespace) {
//     return Object.keys(obj).reduce((prev, cur) => {
//       prev[`${namespace}[${cur}]`] = obj[cur]
//       return prev;
//     }, {})
//   } else {
//     return Object.keys(obj).reduce((prev, cur) => {
//       if (obj[cur] instanceof Object) {
//         return { ...prev, ...toQueryStringObject(obj[cur], cur) };
//       } else {
//         prev[cur] = obj[cur];
//         return prev;
//       }
//     }, {})
//   }
// }

// {
// perPage: 100,
// filters: {
//   contractType: filters.category,
//   name: filters.title,
//   cityId: filters.city,
//   neighbourhoodId: filters.neighborhood,
//   between: {
//     price: [0, filters.price],
//     space: [0, filters.space],
//   },
//   bedroomsNumber: filters.bedrooms,
//   bathroomsNumber: filters.bathrooms
// }
// }

export const toQueryStringObject = (obj) => {

  return Object.keys(obj).reduce((acc, key) => {

    const value = obj[key];

    if (value instanceof Object) {

      Object.keys(value).map(function(innerKey) {

        const innerValue = value[innerKey];

        if(innerValue instanceof Object) {

          Object.keys(innerValue).map(function(inInnerKey) {

            const inInnerValue = innerValue[inInnerKey];

            acc[`${key}[${innerKey}][${inInnerKey}]`] = inInnerValue;

            return ''
          });

        } else {

          acc[`${key}[${innerKey}]`] = innerValue;

        }

        return ''
      });

      return acc;
    } else {
      acc[key] = value;
      return acc;
    }
  }, {});
}

export const isAuthenticated = () => {
  const accessToken = LocalStorage.get('access_token');
  return !!accessToken;
}

export const logout = (e, history) => {
  e.preventDefault();
  localStorage.removeItem('access_token');
  history.push('/');
}

export const imageFallback = (e, placeholder = propertyImagePlaceholder) => {
  e.persist();
  e.target.src = placeholder;
}

export const getPropertyFormatter = (data = {}, t = v => v) => {
  const amenities = Object.keys(data?.amenities ?? {}).map(amenity => ({ label: t(amenity), value: amenity }));
  return {
    images: data.images?.map?.(img => img ? withImagePrefix(img) : propertyImagePlaceholder),
    title: data.name,
    description: data.description,
    city: data.cityId,
    country: data.countryId,
    neighborhood: data.neighbourhoodId,
    bathrooms: bathroomsOptions(t).find(option => option.value === data.bathroomNumber),
    bedrooms: bedroomsOptions(t).find(option => option.value === data.bedroomNumber),
    space: parseFloat(data.space),
    price: parseFloat(data.price),
    category: data.contractType,
    // amenities: Object.keys(data?.amenities ?? {}).map(amenity => amenity),
    amenities,
    location: data.location || {}
  }
}

export function numberFormatter(x) {
	if(isNaN(x)) return x;

	if(x < 9999) {
		return x.toFixed(1);
	}

	if(x < 1000000) {
		return Math.round(x/1000) + "K";
	}
	if( x < 10000000) {
		return (x/1000000).toFixed(2) + "M";
	}

	if(x < 1000000000) {
		return Math.round((x/1000000)) + "M";
	}

	if(x < 1000000000000) {
		return Math.round((x/1000000000)) + "B";
	}

	return "1T+";
}

export const isMobile = new MobileDetect(window.navigator.userAgent).mobile();

export const imageConverter = image => {
  if (image instanceof File) {
    return URL.createObjectURL?.(image);
  } else {
    return image;
  }
}
