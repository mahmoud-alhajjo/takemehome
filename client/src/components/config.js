export const priceSellMarks = {
  0: `0`,
  100000000: `100M`,
  200000000: `200M`,
  300000000: `300M`,
  400000000: `400M`,
  500000000: `500M`,
  600000000: `600M`,
  700000000: `700M`,
  800000000: `800M`,
  900000000: `900M`,
  1000000000: `1B`,
}

export const priceRentMarks = {
  0: `0`,
  100000: `100K`,
  200000: `200K`,
  300000: `300K`,
  400000: `400K`,
  500000: `500K`,
  600000: `600K`,
  700000: `700K`,
  800000: `800K`,
  900000: `900K`,
  1000000: `1M`,
}

export const priceSellMax = 1000000000;
export const priceRentMax = 1000000;

export const spaceMarks = {
  0: `0`,
  100: `100m`,
  200: `200m`,
  300: `300m`,
  400: `400m`,
  500: `500m`,
  600: `600m`,
  700: `700m`,
  800: `800m`,
  900: `900m`,
  1000: `1000m`
}

export const spaceMax = 1000;

export const formatOptionsWithLang = (options, lang) => {
  return options?.map(item => ({ label: item.label[lang], value: item.value }))
}

export const roomsOptions = (t) => ([
  { label: `1 ${t('rooms')}`, value: 1 },
  { label: `2 ${t('rooms')}`, value: 2 },
  { label: `3 ${t('rooms')}`, value: 3 },
  { label: `4 ${t('rooms')}`, value: 4 },
  { label: `4 ${t('rooms')}`, value: 4 },
  { label: `5 ${t('rooms')}`, value: 5 },
  { label: `6 ${t('rooms')}`, value: 6 },
  { label: `7 ${t('rooms')}`, value: 7 },
  { label: `8 ${t('rooms')}`, value: 8 },
  { label: `9 ${t('rooms')}`, value: 9 },
]);

export const bathroomsOptions = (t) => ([
  { label: `0 ${t('bathrooms')}`, value: 0 },
  { label: `1 ${t('bathrooms')}`, value: 1 },
  { label: `2 ${t('bathrooms')}`, value: 2 },
  { label: `3 ${t('bathrooms')}`, value: 3 },
  { label: `4 ${t('bathrooms')}`, value: 4 },
  { label: `4 ${t('bathrooms')}`, value: 4 },
  { label: `5 ${t('bathrooms')}`, value: 5 },
  { label: `6 ${t('bathrooms')}`, value: 6 },
  { label: `7 ${t('bathrooms')}`, value: 7 },
  { label: `8 ${t('bathrooms')}`, value: 8 },
  { label: `9 ${t('bathrooms')}`, value: 9 },
]);

export const bedroomsOptions = (t) => ([
  { label: `0 ${t('bedrooms')}`, value: 0 },
  { label: `1 ${t('bedrooms')}`, value: 1 },
  { label: `2 ${t('bedrooms')}`, value: 2 },
  { label: `3 ${t('bedrooms')}`, value: 3 },
  { label: `4 ${t('bedrooms')}`, value: 4 },
  { label: `4 ${t('bedrooms')}`, value: 4 },
  { label: `5 ${t('bedrooms')}`, value: 5 },
  { label: `6 ${t('bedrooms')}`, value: 6 },
  { label: `7 ${t('bedrooms')}`, value: 7 },
  { label: `8 ${t('bedrooms')}`, value: 8 },
  { label: `9 ${t('bedrooms')}`, value: 9 },
]);

export const amenities = [
  'airConditioning',
  'lawn',
  'dryer',
  'microwave',
  'souna',
  'windowCovering',
  'barbecue',
  'washingMachine',
  'oven',
  'landry',
  'bedroom',
  'bathroom',
  'refrigerator',
  'wifi'
]

export const socialMediaLinks = {
  facebook: 'https://www.facebook.com/WaelZoaiter/',
  twitter: 'https://twitter.com/WZoaiter',
  linkedin: 'https://www.linkedin.com/in/wael-zoaiter/'
}

export const images = {
  cities: {
    1: "/assets/img/explore/1.png",
    2: "/assets/img/explore/2.png",
    3: "/assets/img/explore/3.png",
    4: "/assets/img/explore/4.png",
    5: "/assets/img/explore/1.png",
    6: "/assets/img/explore/2.png",
    7: "/assets/img/explore/3.png",
    8: "/assets/img/explore/4.png",
    9: "/assets/img/explore/1.png",
    10: "/assets/img/explore/2.png",
    11: "/assets/img/explore/3.png",
    12: "/assets/img/explore/4.png",
    13: "/assets/img/explore/1.png",
    14: "/assets/img/explore/2.png",
    15: "/assets/img/explore/3.png",
    16: "/assets/img/explore/4.png",
    17: "/assets/img/explore/1.png",
    18: "/assets/img/explore/2.png",
    19: "/assets/img/explore/3.png",
    20: "/assets/img/explore/4.png",
    21: "/assets/img/explore/1.png",
    22: "/assets/img/explore/2.png",
    23: "/assets/img/explore/3.png",
    24: "/assets/img/explore/4.png",
    25: "/assets/img/explore/1.png",
    26: "/assets/img/explore/2.png"
  },
  houses: {
    1: "/assets/img/cities/damascus.jpg",
    2: "/assets/img/cities/aleppo.jpg",
    3: "/assets/img/cities/homs.jpg",
    4: "/assets/img/cities/hama.jpg",
    5: "/assets/img/cities/latakia.jpg",
    6: "/assets/img/cities/idlb.jpg",
    7: "/assets/img/cities/al_hasake.jpg",
    8: "/assets/img/cities/deir_ez_zor.jpg",
    9: "/assets/img/cities/tartus.jpg",
    10: "/assets/img/cities/al_raqqa.jpg",
    11: "/assets/img/cities/daraa.jpg",
    12: "/assets/img/cities/swaida.jpg",
    13: "/assets/img/cities/13.jpg",
    14: "/assets/img/cities/14.jpg",
  }
}

export const footerList1 = {
  title: 'quick_links',
  items: [
    {
      url: '/about',
      text: 'about'
    },
    {
      url: '/contact',
      text: 'contact_us'
    }
  ]
}

export const footerList2 = {
  title: 'popular_pages',
  items: [
    {
      url: '/search',
      text: 'search'
    },
    {
      url: '/property',
      text: 'properties'
    },
    {
      url: '/news',
      text: 'blog'
    }
  ]
}

export const footerList3 = {
  title: 'explore',
  items: [
    {
      url: '/neighborhoods',
      text: 'neighborhoods'
    },
    {
      url: '/cities',
      text: 'cities'
    }
  ]
}
