import React, { useEffect, useState } from 'react'
import Navbar from '../../global-components/navbar';
import Footer from '../../global-components/footer';
import { MobileHeader, MobileBottomTabBar, SideBar, PageHeader } from '../../global-components';
import MobileDetect from 'mobile-detect';
import { useTranslation } from 'react-i18next';
import { Helmet } from "react-helmet";
import classNames from 'classnames';

const AppLayout = ({ title, children }) => {
  const { t } = useTranslation();
  const [isMobile, setIsMobile] = useState();

  useEffect(() => {
    const md = new MobileDetect(window.navigator.userAgent);
    setIsMobile(md?.mobile());
  }, []);


  const tabs = [
    {
      key: 'home',
      url: '/',
      icon: 'la la-home',
      label: t('home')
    },
    {
      key: 'search',
      url: '/search',
      icon: 'la la-search',
      label: t('search')
    },
    {
      key: 'properties',
      url: '/property',
      icon: 'la la-city',
      label: t('properties')
    },
    {
      key: 'blog',
      url: '/blog',
      icon: 'la la-bold',
      label: t('blog')
    },
  ]

  return (
    <>
      <Helmet titleTemplate={`%s | ${t('sitename')}`}>
        <title>{title || t('home')}</title>
        <meta name="description" content="Find the best houses in Syria" />
        <meta name="author" content="Wael Zoaiter" />
        <meta name="keyword" content="Properties,Houses" />
        <meta property="og:title" content={title || t('home')} />
        <meta property="og:site_name" content={t('sitename')} />
        <meta property="og:description" content="Find the best houses in Syria" />
        <meta property="og:image" content="%PUBLIC_URL%/logo/takemehome-logo-small.png" />
      </Helmet>
      {!isMobile ? (
        <div className={classNames("app-layout", { 'sub-page': !!title })}>
          <Navbar />
          {title && <PageHeader headertitle={title} />}
          {children}
          <Footer />
        </div>
      ) : (
          <div className={classNames("app-layout", { 'sub-page': !!title })} style={{ paddingBottom: 55 }}>
            <MobileHeader
              image={title ? '' : "/logo/takemehome-logo-wide.png"}
              title={title}
              sideBar={SideBar}
            />
            {children}
            <MobileBottomTabBar tabs={tabs} />
          </div>
        )}
    </>
  )
}

export default AppLayout
