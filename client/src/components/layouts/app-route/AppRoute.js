import React from 'react'
import { Route } from 'react-router-dom'
import { AppLayout } from '../app-layout'

const AppRoute = ({
  exact = false,
  path,
  component: Component,
  render,
  children,
  title,
  ...rest
}) => {
  return (
    <Route exact path={path} {...rest}>
      <AppLayout title={title}>
        {children || render || <Component />}
      </AppLayout>
    </Route>
  )
}

export default AppRoute
