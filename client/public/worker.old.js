// Flag for enabling cache in production
// var doCache = false;
// var CACHE_NAME = 'pwa-app-cache';
// // Delete old caches
// self.addEventListener('activate', event => {
//   const currentCachelist = [CACHE_NAME];
//   event.waitUntil(
//     caches.keys()
//       .then(keyList =>
//         Promise.all(keyList.map(key => {
//           if (!currentCachelist.includes(key)) {
//             return caches.delete(key);
//           }
//         }))
//       )
//   );
// });
// // This triggers when user starts the app
// self.addEventListener('install', function (event) {
//   if (doCache) {
//     event.waitUntil(
//       caches.open(CACHE_NAME)
//         .then(function (cache) {
//           // cache.addAll([
//           //   // Logos
//           //   '/logo/takemehome-logo-wide.png',
//           //   '/favicon.ico',
//           //   // Others
//           //   '/assets/img/banner/1.jpg',
//           //   '/assets/img/banner/3.png',
//           //   '/assets/img/bg/1.png',
//           //   '/assets/img/bg/2.png',
//           //   '/assets/img/bg/3.png',
//           //   '/assets/img/bg/4.png',
//           //   '/assets/img/bg/5.png',
//           //   '/assets/img/bg/6.png',
//           //   '/assets/img/city/1.png',
//           //   '/assets/img/city/2.png',
//           //   '/assets/img/city/3.png',
//           //   '/assets/img/city/4.png',
//           //   '/assets/img/city/5.png',
//           //   '/assets/img/city/6.png',
//           //   '/assets/img/city/7.png',
//           //   '/assets/img/explore/1.png',
//           //   '/assets/img/explore/2.png',
//           //   '/assets/img/explore/3.png',
//           //   '/assets/img/explore/4.png',
//           //   '/assets/img/news/2.jpg',
//           //   // Icons
//           //   '/assets/img/icons/1.png',
//           //   '/assets/img/icons/2.png',
//           //   '/assets/img/icons/3.png',
//           //   '/assets/img/icons/4.png',
//           //   '/assets/img/icons/5.png',
//           //   '/assets/img/icons/6.png',
//           //   '/assets/img/icons/7.png',
//           //   '/assets/img/icons/8.png',
//           //   '/assets/img/icons/9.png',
//           //   '/assets/img/icons/10.png',
//           //   '/assets/img/icons/11.png',
//           //   '/assets/img/icons/12.png',
//           //   '/assets/img/icons/13.png',
//           //   '/assets/img/icons/14.png',
//           //   '/assets/img/icons/15.png',
//           //   '/assets/img/icons/16.png',
//           //   '/assets/img/icons/17.png',
//           //   '/assets/img/icons/18.png',
//           //   '/assets/img/icons/19.png',
//           //   '/assets/img/icons/20.png',
//           //   '/assets/img/icons/21.png',
//           //   '/assets/img/icons/22.png',
//           //   '/assets/img/icons/23.png',
//           //   '/assets/img/icons/24.png',
//           //   '/assets/img/icons/25.png',
//           //   '/assets/img/icons/26.png',
//           //   '/assets/img/icons/27.png',
//           //   '/assets/img/icons/28.png',
//           //   '/assets/img/icons/29.png',
//           //   '/assets/img/icons/30.png',
//           //   '/assets/img/icons/31.png',
//           //   // Placeholders
//           //   '/assets/img/user-placeholder.png',
//           //   '/assets/img/property-placeholder.png'
//           // ]);
//         })
//     );
//   }
// });
// // Here we intercept request and serve up the matching files
// self.addEventListener('fetch', function (event) {
//   if (doCache) {
//     event.respondWith(
//       caches.match(event.request).then(function (response) {
//         return response || fetch(event.request);
//       })
//     );
//   }
// });
