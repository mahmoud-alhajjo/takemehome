/**
 * Install Prompt
 */

let deferredPrompt;

self.addEventListener('beforeinstallprompt', (e) => {
  // Prevent the mini-infobar from appearing on mobile
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Update UI notify the user they can install the PWA
  console.log('SW event: beforeinstallprompt', e);
});

// var buttonInstall = document.getElementById('show-install-banner');

self.addEventListener('load', (e) => {
  setTimeout(function () {
    // Show the install prompt
    deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    deferredPrompt.userChoice.then((choiceResult) => {
      if (choiceResult.outcome === 'accepted') {
        console.log('User accepted the install prompt');
      } else {
        console.log('User dismissed the install prompt');
      }
    });
  }, 31000)
});

self.addEventListener('appinstalled', (evt) => {
  // Log install to analytics
  console.log('INSTALL: Success');
});

/**
 * Push Notification
 */

self.addEventListener('push', function (e) {
  var options = {
    body: 'This notification was generated from a push!',
    icon: '/logo/takemehome-logo-small.png',
    vibrate: [100, 50, 100],
    data: {
      dateOfArrival: Date.now(),
      primaryKey: '2'
    },
    actions: [
      {
        action: 'explore', title: 'Explore',
        icon: '/assets/img/icons/6.png'
      },
      {
        action: 'close', title: 'Close',
        icon: 'images/xmark.png'
      },
    ]
  };
  if (!("Notification" in self)) {
    console.log("This browser does not support desktop notification");
  }

  // Let's check whether notification permissions have already been granted
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    e.waitUntil(
      self.registration.showNotification('Hello world!', options)
    );
  }

  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== "denied") {
    Notification.requestPermission().then(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        e.waitUntil(
          self.registration.showNotification('Hello world!', options)
        );
      }
    });
  }
});

/**
 * Workbox
 */

importScripts('https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js');


if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉`);

  const { registerRoute } = workbox.routing;
  const { CacheFirst, NetworkFirst, NetworkOnly } = workbox.strategies;
  const { CacheableResponse } = workbox.cacheableResponse;
  const { precacheAndRoute } = workbox.precaching;

  registerRoute(
    ({ request }) => request.destination === 'script',
    new NetworkFirst()
  );

  registerRoute(
    ({ request }) => request.destination === 'image',
    new CacheFirst({
      plugins: [
        new CacheableResponse({ statuses: [0, 200] })
      ],
    })
  );

  precacheAndRoute([
    // Logos
    { url: '/logo/takemehome-logo-wide.png', revision: null },
    { url: '/favicon.ico', revision: null },
    // Icons
    { url: '/assets/img/icons/1.png', revision: null },
    { url: '/assets/img/icons/2.png', revision: null },
    { url: '/assets/img/icons/3.png', revision: null },
    { url: '/assets/img/icons/4.png', revision: null },
    { url: '/assets/img/icons/5.png', revision: null },
    { url: '/assets/img/icons/6.png', revision: null },
    { url: '/assets/img/icons/7.png', revision: null },
    { url: '/assets/img/icons/8.png', revision: null },
    { url: '/assets/img/icons/9.png', revision: null },
    { url: '/assets/img/icons/10.png', revision: null },
    { url: '/assets/img/icons/11.png', revision: null },
    { url: '/assets/img/icons/12.png', revision: null },
    { url: '/assets/img/icons/13.png', revision: null },
    { url: '/assets/img/icons/14.png', revision: null },
    { url: '/assets/img/icons/15.png', revision: null },
    { url: '/assets/img/icons/16.png', revision: null },
    { url: '/assets/img/icons/17.png', revision: null },
    { url: '/assets/img/icons/18.png', revision: null },
    { url: '/assets/img/icons/19.png', revision: null },
    { url: '/assets/img/icons/20.png', revision: null },
    { url: '/assets/img/icons/21.png', revision: null },
    { url: '/assets/img/icons/22.png', revision: null },
    { url: '/assets/img/icons/23.png', revision: null },
    { url: '/assets/img/icons/24.png', revision: null },
    { url: '/assets/img/icons/25.png', revision: null },
    { url: '/assets/img/icons/26.png', revision: null },
    { url: '/assets/img/icons/27.png', revision: null },
    { url: '/assets/img/icons/28.png', revision: null },
    { url: '/assets/img/icons/29.png', revision: null },
    { url: '/assets/img/icons/30.png', revision: null },
    { url: '/assets/img/icons/31.png', revision: null },
    // Placeholders
    { url: '/assets/img/user-placeholder.png', revision: null },
    { url: '/assets/img/property-placeholder.png', revision: null },
  ]);

  self.addEventListener('fetch', (event) => {
    const networkFirst = new NetworkFirst();
    event.respondWith(networkFirst.handle({ request: event.request }));
  });

  /**
   * BackgroundSync
   */

  const {BackgroundSyncPlugin} = workbox.backgroundSync;

  const bgSyncPlugin = new BackgroundSyncPlugin('apiQueue', {
    maxRetentionTime: 24 * 60 // Retry for max of 24 Hours (specified in minutes)
  });

  registerRoute(
    /\/api\/*/,
    new NetworkOnly({
      plugins: [bgSyncPlugin]
    }),
    'GET'
  );
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}
