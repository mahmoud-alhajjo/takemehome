import React from 'react';

/**
 * Main Pages
 */

const PropertiesList = React.lazy(() => import('./views/properties/PropertiesList'));
const EditProperty = React.lazy(() => import('./views/properties/edit-property/EditProperty'));
const AddProperty = React.lazy(() => import('./views/properties/add-property/AddProperty'));

const CitiesList = React.lazy(() => import('./views/cities/CitiesList'));
const EditCity = React.lazy(() => import('./views/cities/edit-city/EditCity'));
const AddCity = React.lazy(() => import('./views/cities/add-city/AddCity'));

const CountriesList = React.lazy(() => import('./views/countries/CountriesList'));
const EditCountry = React.lazy(() => import('./views/countries/edit-country/EditCountry'));
const AddCountry = React.lazy(() => import('./views/countries/add-country/AddCountry'));

const NeighborhoodsList = React.lazy(() => import('./views/neighborhoods/NeighborhoodsList'));
const EditNeighborhood = React.lazy(() => import('./views/neighborhoods/edit-neighborhood/EditNeighborhood'));
const AddNeighborhood = React.lazy(() => import('./views/neighborhoods/add-neighborhood/AddNeighborhood'));

const SettingsList = React.lazy(() => import('./views/settings/SettingsList'));
const EditSetting = React.lazy(() => import('./views/settings/edit-setting/EditSetting'));
const AddSetting = React.lazy(() => import('./views/settings/add-setting/AddSetting'));

const ArticlesList = React.lazy(() => import('./views/articles/ArticlesList'));
const EditArticle = React.lazy(() => import('./views/articles/edit-article/EditArticle'));
const AddArticle = React.lazy(() => import('./views/articles/add-article/AddArticle'));

const UsersList = React.lazy(() => import('./views/user/UsersList'));
const EditUser = React.lazy(() => import('./views/user/edit-user/EditUser'));
const AddUser = React.lazy(() => import('./views/user/add-user/AddUser'));

/**
 * Other Pages
 */
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const CoreUIIcons = React.lazy(() => import('./views/Icons/CoreUIIcons'));
const Flags = React.lazy(() => import('./views/Icons/Flags'));
const FontAwesome = React.lazy(() => import('./views/Icons/FontAwesome'));
const SimpleLineIcons = React.lazy(() => import('./views/Icons/SimpleLineIcons'));
const Widgets = React.lazy(() => import('./views/Widgets/Widgets'));
const Users = React.lazy(() => import('./views/Users/Users'));
const User = React.lazy(() => import('./views/Users/User'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  // Properties
  { path: '/properties', name: 'Properties', component: PropertiesList },
  { path: '/edit-property/:propertyId', name: 'Edit Property', component: EditProperty },
  { path: '/add-property', name: 'Add Property', component: AddProperty },
  // Cities
  { path: '/cities', name: 'Cities', component: CitiesList },
  { path: '/edit-city/:cityId', name: 'Edit City', component: EditCity },
  { path: '/add-city', name: 'Add City', component: AddCity },
  // Countries
  { path: '/countries', name: 'Countries', component: CountriesList },
  { path: '/edit-country/:countryId', name: 'Edit Country', component: EditCountry },
  { path: '/add-country', name: 'Add Country', component: AddCountry },
  // Neighborhoods
  { path: '/neighborhoods', name: 'Neighborhoods', component: NeighborhoodsList },
  { path: '/edit-neighborhood/:neighborhoodId', name: 'Edit Neighborhood', component: EditNeighborhood },
  { path: '/add-neighborhood', name: 'Add Neighborhood', component: AddNeighborhood },
  // Settings
  { path: '/settings', name: 'Settings', component: SettingsList },
  { path: '/edit-setting/:settingId', name: 'Edit Setting', component: EditSetting },
  { path: '/add-setting', name: 'Add Setting', component: AddSetting },
  // Articles
  { path: '/articles', name: 'Articles', component: ArticlesList },
  { path: '/edit-article/:articleId', name: 'Edit Article', component: EditArticle },
  { path: '/add-article', name: 'Add Article', component: AddArticle },
  // Users
  { path: '/users', name: 'Users', component: UsersList },
  { path: '/edit-user/:userId', name: 'Edit User', component: EditUser },
  { path: '/add-user', name: 'Add User', component: AddUser },

  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/font-awesome', name: 'Font Awesome', component: FontAwesome },
  { path: '/icons/simple-line-icons', name: 'Simple Line Icons', component: SimpleLineIcons },
  { path: '/widgets', name: 'Widgets', component: Widgets },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
];

export default routes;
