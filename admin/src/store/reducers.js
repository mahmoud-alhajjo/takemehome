import { combineReducers } from 'redux'

const initialAuthState = {
  accessToken: localStorage.getItem('access_token'),
  userInfo: localStorage.getItem('user_info') ? JSON.parse(localStorage.getItem('user_info')) : null,
  showAuthModal: false
}

function authReducer(state = initialAuthState, action) {
  switch (action.type) {
    case 'SHOW_MODAL':
      return {
        ...state,
        showAuthModal: action.payload.showAuthModal
      };
    case 'LOGIN':
      return {
        ...state,
        showAuthModal: false,
        accessToken: action.payload.accessToken,
        userInfo: action.payload.userInfo
      };
    case 'LOGOUT':
      return {
        ...state,
        accessToken: ''
      };
    default:
      return state
  }
}

const rootReducer = combineReducers({
  auth: authReducer
})

export default rootReducer
