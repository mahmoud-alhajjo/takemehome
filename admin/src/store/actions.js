export const showAuthModalAction = (data) => {
  return {
    type: 'SHOW_MODAL',
    payload: {
      showAuthModal: data.showAuthModal
    }
  }
}

export const loginAction = (data) => {
  return {
    type: 'LOGIN',
    payload: { 
      accessToken: data.accessToken,
      userInfo: data.userInfo
    }
  }
}

export const logoutAction = () => {
  localStorage.removeItem('access_token');
  localStorage.removeItem('user_info');

  return {
    type: 'LOGOUT',
    payload: { accessToken: '', showAuthModal: false, userInfo: '' }
  }
}
