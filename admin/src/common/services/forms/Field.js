import React from 'react'
import { FormGroup, FormFeedback, Input, Label } from 'reactstrap'
import { Controller } from 'react-hook-form'
import classNames from 'classnames'

const Field = ({
 name,
 placeholder,
 className,
 component: Component = Input,
 errors = {},
 label,
 control,
 rules = {},
 tag = "",
 defaultValue = "",
 ...rest
}) => {
  if (tag) Component = <Component tag={tag} />
  
  rest = rest.options ? {
    ...rest,
    classNamePrefix: "react-select"
  } : rest;

  return (
    <FormGroup controlId={name} className={classNames("form-field", className)}>
      {label && 
        <Label for={name}>{label}</Label>}
      <Controller
        name={name}
        className={classNames({ "is-invalid": errors[name] })}
        as={Component}
        classNamePrefix={rest.options ? "react-select" : undefined}
        placeholder={placeholder}
        control={control}
        rules={rules}
        defaultValue={defaultValue}
        {...rest}
      />
      {errors[name] &&
        <FormFeedback type="invalid">{errors[name]?.message}</FormFeedback>}
    </FormGroup>
  )
}

export default Field
