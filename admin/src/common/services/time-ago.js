import TimeAgo from 'javascript-time-ago'
import en from 'javascript-time-ago/locale/en'
 
// Add locale-specific relative date/time formatting rules.
TimeAgo.addLocale(en)

export const timeAgo = dateTime => {
  const time = new TimeAgo('en-US')
  
  try {
    return time.format(new Date(dateTime));
  } catch {
    return null;
  }
}