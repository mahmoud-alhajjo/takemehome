const LocalStorage = {
  get(key) {
    const item = localStorage.getItem(key);

    return item ? JSON.parse(item) : null;
  },

  set(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
  }
}

export default LocalStorage;
