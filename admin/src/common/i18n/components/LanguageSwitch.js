import React from 'react'
import { useTranslation } from 'react-i18next'
import { Button } from 'react-bootstrap'
import classNames from 'classnames'
import $ from 'jquery'

const LanguageSwitch = ({ lang }) => {
  const { i18n } = useTranslation();
  const currentLanguage = i18n.language;
  const otherLanguage = currentLanguage === 'en' ? 'ar' : 'en';
  const direction = currentLanguage === 'en' ? 'ltr' : 'rtl';
  const otherDirection = direction === 'ltr' ? 'rtl' : 'ltr';
  const label = currentLanguage === 'ar' ? 'English' : 'عربي';
  const className = currentLanguage === 'ar' ? '' : 'arabic-font';

  const handleLanguageChange = () => {
    i18n.changeLanguage(otherLanguage, () => {
      $('html').attr('lang', currentLanguage);
      $('body').addClass(otherDirection).removeClass(direction);
    });
  }

  return (
    <Button 
      onClick={handleLanguageChange}
      variant="dark"
      className={classNames("language-switch", className, lang)}
    >{label}</Button>
  )
}

export default LanguageSwitch
