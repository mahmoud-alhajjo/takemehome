import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import arabic from './locales/arabic.json';
import english from './locales/english.json';

i18next
  // .use(Fetch)
  .use(initReactI18next)
  .init({
    fallbackLng: 'en',
    preload: ['en'],
    lng: 'en',
    resources: {
      en: { translation: english },
      ar: { translation: arabic },
    },
    react: {
      bindI18n: 'languageChanged',
      transEmptyNodeValue: '',
      transSupportBasicHtmlNodes: true,
      transKeepBasicHtmlNodesFor: ['br', 'strong', 'i'],
      useSuspense: false
    }
  })

export default i18next