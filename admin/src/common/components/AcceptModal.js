import SweetAlert from './SweetAlert';
import { API } from '../../common';
import cogoToast from 'cogo-toast';

const acceptModal = async (url, options = {}) => {
  return SweetAlert.fire({
    icon: 'question',
    title: 'Are you sure you want to accept this data?',
    showCancelButton: true,
    confirmButtonText: 'Accept',
    cancelButtonText: 'Cancel',
    showLoaderOnConfirm: true,
    ...options,
    preConfirm: () => {
      return API.get(url)
        .then(response => {
          if (response.status >= 300) {
            throw new Error(response.statusText)
          }
          return response
        })
        .catch(error => {
          SweetAlert.showValidationMessage(
            `Request failed: ${error}`
          )
        })
    },
    allowOutsideClick: () => !SweetAlert.isLoading()
  }).then((result) => {
    if (result?.value?.data?.code < 300) {
      SweetAlert.close();
      cogoToast.success(options.successMessage || 'Data acceptd Successfully!');
      return Promise.resolve(result);
    }
  })
}

export default acceptModal
