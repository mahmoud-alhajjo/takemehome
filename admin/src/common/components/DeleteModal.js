import SweetAlert from './SweetAlert';
import { API } from '../../common';
import cogoToast from 'cogo-toast';

const deleteModal = async (url, options = {}) => {
  return SweetAlert.fire({
    icon: 'error',
    title: 'Are you sure you want to delete this data?',
    showCancelButton: true,
    confirmButtonText: 'Delete',
    cancelButtonText: 'Cancel',
    showLoaderOnConfirm: true,
    ...options,
    preConfirm: () => {
      return API.delete(url)
        .then(response => {
          if (response.status >= 300) {
            throw new Error(response.statusText)
          }
          return response
        })
        .catch(error => {
          SweetAlert.showValidationMessage(
            `Request failed: ${error}`
          )
        })
    },
    allowOutsideClick: () => !SweetAlert.isLoading()
  }).then((result) => {
    if (result?.value?.data?.code < 300) {
      SweetAlert.close();
      cogoToast.success('Data deleted Successfully!');
      return Promise.resolve(result);
    }
  })
}

export default deleteModal
