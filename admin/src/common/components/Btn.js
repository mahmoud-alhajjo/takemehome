import React from 'react'
import { Button, Spinner } from 'reactstrap'

const Loader = (props) => {
  return (
    <Spinner
      animation="border"
      variant="light"
      size="sm"
      className="my-auto d-block"
      {...props}
    />
  )
}

const Btn = ({
  children,
  loading = false,
  loaderProps = {},
  variant = "primary",
  ...props
}) => {
  return (
    <Button
      disabled={loading}
      color={variant}
      {...props}
    >{loading ? <Loader {...loaderProps} /> : children}</Button>
  )
}

export default Btn
