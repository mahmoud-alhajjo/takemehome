import React from 'react'
import ContentLoader from 'react-content-loader'

export const PlaceholderHorizontalCard = () => (
  <ContentLoader 
    speed={2}
    width={600}
    height={200}
    viewBox="0 0 600 200"
    backgroundColor="#ddd"
    foregroundColor="#ecebeb"
  >
    <rect x="192" y="22" rx="2" ry="2" width="140" height="16" /> 
    <rect x="193" y="66" rx="2" ry="2" width="279" height="10" /> 
    <rect x="26" y="18" rx="2" ry="2" width="150" height="150" /> 
    <rect x="193" y="86" rx="2" ry="2" width="234" height="9" /> 
    <rect x="193" y="145" rx="2" ry="2" width="150" height="9" /> 
    <rect x="194" y="127" rx="2" ry="2" width="126" height="8" />
  </ContentLoader>
)

export const PlaceholderVerticalCard = () => (
  <ContentLoader
    speed={.5}
    width={200}
    height={300}
    viewBox="0 0 200 300"
    backgroundColor="#ddd"
    foregroundColor="#ecebeb"
  >
    <rect x="0" y="0" rx="5" ry="5" width="170" height="170" />
    <rect x="0" y="190" rx="2" ry="2" width="170" height="12" />
    <rect x="0" y="210" rx="2" ry="2" width="170" height="12" />
    <rect x="0" y="230" rx="2" ry="2" width="170" height="12" />
  </ContentLoader>
)

export const PlaceholderSquareCard = () => (
  <ContentLoader
    speed={.5}
    width={200}
    height={300}
    viewBox="0 0 200 300"
    backgroundColor="#ddd"
    foregroundColor="#ecebeb"
  >
    <rect x="0" y="0" rx="5" ry="5" width="170" height="170" />
  </ContentLoader>
)

export const PlaceholderSection = ({
  vertical = false,
  square = false
}) => {
  const Component = square ? PlaceholderSquareCard : vertical ?  PlaceholderVerticalCard : PlaceholderHorizontalCard;
  return (
    <div className="text-center">
      <Component />
      <Component />
      <Component />
      <Component />
    </div>
  )
}
