export const dtoToDatatable = (dtoData, customProps = {}) => {
  return Object.keys(dtoData).map(key => {
    const value = dtoData[key];

    const custom = customProps[key] || {}

    return {
      Header: value.label,
      accessor: key,
      ...custom
    }
  })
}

export const objToArr = obj => Object.keys(obj).filter(key => !!obj[key])