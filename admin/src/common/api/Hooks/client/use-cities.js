import useAxios from 'axios-hooks'
import { Endpoints } from "../../Endpoints"
import { useEffect } from 'react';
import cogoToast from 'cogo-toast';

export const useCities = (filters = { perPage: 20 }) => {
  const [{ 
    data: { content: { data: cities } = {} } = {},
    loading,
    error
  }] = useAxios({
    url: Endpoints.client.cities,
    params: filters
  });

  useEffect(() => {
    console.log(error);
    if (error) cogoToast.error(error?.message)
  }, [error])

  return {
    data: cities?.map(selectOptionFormatter),
    loading
  }
}

const selectOptionFormatter = ({ id, name, countryId, countryName }) => {
  return {
    label: name,
    value: id,
    countryId, 
    countryName
  }
}
