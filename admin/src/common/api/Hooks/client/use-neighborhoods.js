import useAxios from 'axios-hooks'
import { Endpoints } from "../../Endpoints"
import { useEffect } from 'react';
import cogoToast from 'cogo-toast';
import { toQueryStringObject } from '../../../../components/helpers';

export const useNeighborhoods = (filters = { perPage: 20 }) => {
  const [{ 
    data: { content: { data: neighborhoods } = {} } = {}, 
    loading, 
    error 
  }] = useAxios({
    url: Endpoints.client.neighbourhoods,
    params: toQueryStringObject(filters)
  });

  useEffect(() => {
    if (error) cogoToast.error(error?.message)
  }, [error])

  return {
    data: neighborhoods?.map(selectOptionFormatter),
    loading
  }
}

const selectOptionFormatter = ({ id, name, cityId, cityName }) => {
  return {
    label: name,
    value: id,
    cityId, 
    cityName
  }
}
