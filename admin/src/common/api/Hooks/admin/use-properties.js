import useAxios from "axios-hooks"
import { Endpoints } from "../../Endpoints"
import { toQueryStringObject } from "../../../../components/helpers";

export const useAdminProperties = (config = {}, options = {}) => {
  const response = useAxios({
    ...config,
    params: config.params ? toQueryStringObject(config.params) : undefined,
    url: config.id ? `${Endpoints.admin.properties}/${config.id}` : Endpoints.admin.properties,
  }, { useCache: false , ...options });

  return response;
}
