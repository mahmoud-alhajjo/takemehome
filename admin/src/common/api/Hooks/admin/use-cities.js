import useAxios from "axios-hooks"
import { Endpoints } from "../../Endpoints"
import { toQueryStringObject } from "../../../../components/helpers";

export const useAdminCities = (config = {}, options = {}) => {
  const response = useAxios({
    params: config.id ? undefined : toQueryStringObject({
      perPage: 700,
      page:1,
      filters: {},
      orderBy: "id",
      orderByDirection: "desc"
    }),
    ...config,
    url:  config.id ? `${Endpoints.admin.cities}/${config.id}` : Endpoints.admin.cities,
  }, { useCache: false , ...options });

  return response;
}
