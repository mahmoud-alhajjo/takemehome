import useAxios from "axios-hooks"
import { Endpoints } from "../../Endpoints"
import { toQueryStringObject } from "../../../../components/helpers";

export const useAdminUsers = (config = {}, options = {}) => {
  const response = useAxios({
    params: config.id ? undefined : toQueryStringObject({
      perPage: 700,
      page:1,
      filters: {},
      orderBy: "id",
      orderByDirection: "desc"
    }),
    ...config,
    url:  config.id ? `${Endpoints.admin.users}/${config.id}` : Endpoints.admin.users,
  }, { useCache: false , ...options });

  return response;
}
