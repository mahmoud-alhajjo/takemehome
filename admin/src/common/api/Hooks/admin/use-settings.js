import useAxios from "axios-hooks"
import { Endpoints } from "../../Endpoints"
import { toQueryStringObject } from "../../../../components/helpers";

export const useAdminSettings = (config = {}, options = {}) => {
  const response = useAxios({
    params: config.id ? undefined : toQueryStringObject({
      perPage: 700,
      page:1,
      filters: {},
      orderBy: "id",
      orderByDirection: "desc"
    }),
    ...config,
    url:  config.id ? `${Endpoints.admin.settings}/${config.id}` : Endpoints.admin.settings,
  }, { useCache: false , ...options });

  return response;
}
