const BaseUrl = 'https://takemehome12.herokuapp.com';

const ApiBaseUrl = BaseUrl + '/api'

export const Endpoints = {
    baseUrl: ApiBaseUrl,
    admin: {
        articles: '/admin/articles',
        settings: '/admin/settings',
        countries: '/admin/countries',
        cities: '/admin/cities',
        neighbourhoods: '/admin/neighbourhoods',
        properties: '/admin/properties',
        jobRequests: '/admin/jobRequests',
        users: '/admin/users',
        enableUsers: '/admin/users/enable',
        disableUsers: '/admin/users/disable'
    },
    client: {
        articles: '/site/articles',
        settings: '/site/settings',
        properties: '/site/properties',
        jobRequests: '/site/jobRequests',
        neighbourhoods: '/site/neighbourhoods',
        countries: '/site/countries',
        cities: '/site/cities'
    },
    auth: {
        register: '/auth/register',
        login: '/auth/login',
        user: '/auth/user',
        updateProfile: '/auth/updateProfile',
        changePassword: '/auth/changePassword',
        logout: '/auth/logout',
        logoutAllDevices: '/auth/logoutAllDevices',
        confirmAccount: '/auth/conformAccount'
    }
}