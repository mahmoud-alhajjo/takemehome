import axios from 'axios'
import { languages } from "../Endpoints"

export const fetchLanguageStrings = (lang, callback = console.log) => {
    callback({ loading: true })
    axios.get(languages, {
      params: { lang }
    })
    .then(
      data => callback({ loading: false, data: data }),
      error => callback({ loading: false, error })
    )
    .catch(error => callback({ loading: false, error }));
}