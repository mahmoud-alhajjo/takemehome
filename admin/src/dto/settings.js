export const settingDto = {
  id: {
    type: Number,
    label: 'id'
  },
  key: {
    type: String,
    label: 'Key'
  },
  value: {
    type: String,
    label: 'Value'
  },
}
