export const AmenitiesEnum = {
  airConditioning: Boolean,
  lawn: Boolean,
  dryer: Boolean,
  microwave: Boolean,
  souna: Boolean,
  windowCovering: Boolean,
  barbecue: Boolean,
  washingMachine: Boolean,
  oven: Boolean,
  landry: Boolean,
  bedroom: Boolean,
  bathroom: Boolean,
  refrigerator: Boolean,
  wifi: Boolean
}

export const propertyDto = {
  id: {
    type: Number,
    label: 'Id'
  },
  name: {
    type: String,
    label: 'Name'
  },
  status: {
    type: String,
    label: 'Status'
  },
  description: {
    type: String,
    label: 'Description'
  },
  location: {
    type: String,
    label: 'Location'
  },
  bathroomNumber: {
    type: Number,
    label: 'Bathrooms'
  },
  bedroomNumber: {
    type: Number,
    label: 'Bedrooms'
  },
  space: {
    type: String,
    label: 'Space'
  },
  price: {
    type: String,
    label: 'Price'
  },
  contractType: {
    type: String,
    label: 'ContractType'
  },
  views: {
    type: Number,
    label: 'Views'
  },
  userName: {
    type: String,
    label: 'UserName'
  },
  countryName: {
    type: String,
    label: 'CountryName'
  },
  cityName: {
    type: String,
    label: 'CityName'
  },
  neighbourhoodName: {
    type: String,
    label: 'NeighbourhoodName'
  },
  images: {
    type: Array,
    label: 'Images'
  },
  amenities: {
    type: Object,
    enum: AmenitiesEnum,
    label: 'Amenities'
  },
  createdAt: {
    type: Date,
    label: 'CreatedAt'
  },
  updatedAt: {
    type: Date,
    label: 'UpdatedAt'
  }
}
