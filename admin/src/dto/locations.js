export const LangEnum = {
  en: String,
  ar: String
}

export const cityDto = {
  id: {
    type: Number,
    label: 'id'
  },
  name: {
    type: Object,
    enum: LangEnum,
    label: 'name'
  },
  countryId: {
    type: Number,
    label: 'Country ID',
    readonly: true
  },
  countryName: {
    type: String,
    label: 'Country Name'
  },
  createdAt: {
    type: Date,
    label: 'CreatedAt'
  },
  updatedAt: {
    type: Date,
    label: 'UpdatedAt'
  }
}

export const neighborhoodDto = {
  id: {
    type: Number,
    label: 'id'
  },
  name: {
    type: Object,
    enum: LangEnum,
    label: 'name'
  },
  cityId: {
    type: Number,
    label: 'City ID',
    readonly: true
  },
  cityName: {
    type: String,
    label: 'City Name'
  },
  createdAt: {
    type: Date,
    label: 'CreatedAt'
  },
  updatedAt: {
    type: Date,
    label: 'UpdatedAt'
  }
}

export const countryDto = {
  id: {
    type: Number,
    label: 'id'
  },
  name: {
    type: Object,
    enum: LangEnum,
    label: 'name'
  },
  code: {
    type: String,
    label: 'Country Code'
  },
  createdAt: {
    type: Date,
    label: 'CreatedAt'
  },
  updatedAt: {
    type: Date,
    label: 'UpdatedAt'
  }
}
