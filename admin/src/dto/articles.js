export const articleDto = {
  id: {
    type: Number,
    label: 'id'
  },
  name: {
    type: String,
    label: 'Name'
  },
  content: {
    type: String,
    label: 'Content'
  }
}
