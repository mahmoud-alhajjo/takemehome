export const userDto = {
  id: {
    type: String,
    label: 'id'
  },
  name: {
    type: String,
    label: 'Name'
  },
  email: {
    type: String,
    label: 'Email'
  },
  role: {
    type: String,
    label: 'Role'
  },
  phone: {
    type: String,
    label: 'Phone'
  },
  type: {
    type: String,
    label: 'Type'
  },
  address: {
    type: String,
    label: 'Address'
  },
  officeName: {
    type: String,
    label: 'OfficeName'
  },
  enabledAt: {
    type: Date,
    label: 'EnabledAt'
  },
  verifiedAt: {
    type: Date,
    label: 'VerifiedAt'
  },
  createdAt: {
    type: Date,
    label: 'CreatedAt'
  },
  updatedAt: {
    type: Date,
    label: 'UpdatedAt'
  },
}
