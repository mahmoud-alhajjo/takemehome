import React from 'react';
import { timeAgo } from "../common";

export const dateCustomColumns = {
  createdAt: {
    Cell: ({ value }) => <span>{timeAgo(value)}</span>
  },
  updatedAt: {
    Cell: ({ value }) => <span>{timeAgo(value)}</span>
  }
}
