import Dashboard from './Dashboard';
import { CoreUIIcons, Flags, FontAwesome, SimpleLineIcons } from './Icons';
import { Login, Page404, Page500, Register } from './Pages';
import Widgets from './Widgets';

export {
  CoreUIIcons,
  Page404,
  Page500,
  Register,
  Login,
  Flags,
  SimpleLineIcons,
  FontAwesome,
  Dashboard,
  Widgets
};
