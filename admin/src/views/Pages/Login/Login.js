import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Link, useHistory } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Row } from 'reactstrap';
import classNames from 'classnames';
import { isEmpty } from 'ramda';
import { useLogin, Field } from '../../../common';
import Btn from '../../../common/components/Btn';
import { useDispatch } from 'react-redux';
import { loginAction } from '../../../store';

const Login = ({
  onSuccess = () => null
}) => {
  const [validated, setValidated] = useState(false);
  const { handleSubmit, control, errors } = useForm();
  const [{ loading, data }, loginUser] = useLogin();
  const history = useHistory();
  const dispatch = useDispatch();

  const onSubmit = values => {
    setValidated(isEmpty(errors));
    loginUser({ data: values });
  };

  useEffect(() => {
    if (data?.code === 200) {
      dispatch(loginAction({ accessToken: data?.content?.token }))
      history.push('/');
    };
  }, [data, onSuccess, dispatch, history]);

  return (
    <div className="app flex-row align-items-center">
      <Container>
        <Row className="justify-content-center">
          <Col md="8">
            <CardGroup>
              <Card className="p-4">
                <CardBody>
                  <Form
                    className={classNames("contact-form-wrap", "contact-form-bg")}
                    onSubmit={handleSubmit(onSubmit)}
                    validated={validated}
                    noValidate
                  >
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <Field
                      name="email"
                      type="email"
                      placeholder={'Email'}
                      control={control}
                      errors={errors}
                      rules={{
                        required: 'Required',
                        pattern: {
                          value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                          message: 'Invalid Email'
                        }
                      }}
                    />
                    <Field
                      name="password"
                      type="password"
                      placeholder={'Password'}
                      control={control}
                      errors={errors}
                      rules={{ required: 'Required' }}
                    />
                    <div className="btn-wrap">
                      <Btn
                        variant="yellow"
                        type="submit"
                        loading={loading}
                      >Login</Btn>
                    </div>
                  </Form>
                </CardBody>
              </Card>
              <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                <CardBody className="text-center">
                  <div>
                    <h2>Visit Our Application</h2>
                    <p>If you are not an admin or you don't have an account, Visit our application now!</p>
                    <Link to="https://takemehome.netlify.app/">
                      <Button color="primary" className="mt-3" active tabIndex={-1}>Visit Now!</Button>
                    </Link>
                  </div>
                </CardBody>
              </Card>
            </CardGroup>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Login;
