import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { Endpoints, useAdminCountries } from '../../common';
import { dtoToDatatable } from '../../common/helpers';
import { Table } from '../../components';
import { countryDto } from '../../dto';
import { dateCustomColumns } from '../../utils';

const CountriesList = () => {
  const [{ data: { content: { data: countries } = {} } = {}, loading }, refetch] = useAdminCountries();

  return (
    <div className="countries-list">
      <div className="d-flex justify-content-end mb-3">
        <Button color="primary" tag={Link} to="/add-country">Add new country</Button>
      </div>
      <Table
        data={countries}
        columns={dtoToDatatable(countryDto, customColumns)}
        loading={loading}
        actions={{ 
          delete: { 
            onSuccess: () => refetch(), 
            urlPrefix: Endpoints.admin.countries 
          }, 
          edit: { 
            urlPrefix: '/edit-country' 
          }, 
          accept: { 
            hidden: true 
          } 
        }}
      />
    </div>
  )
}

const customColumns = { 
  name: { 
    Cell: ({ value }) => <span>{value.en} ({value.ar})</span>
  },
  ...dateCustomColumns
}

export default CountriesList
