import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { CountryForm } from '../country-form'
import { useAdminCountries } from '../../../common';

const EditCountry = () => {
  const history = useHistory();
  const { countryId } = useParams();

  const [{ loading, data }, editCountry] = useAdminCountries({ method: 'POST', id: countryId }, { manual: true });
  const [{ loading: initialValuesLoading, data: { content: { data: initialValuesData } = {} } = {} }] = useAdminCountries({ id: countryId });

  const onSubmit = (values) => {
    editCountry({
      data: values
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/countries");
    }
  }, [data]);

  return (
    <div className="edit-countries">
      <CountryForm
        onSubmit={onSubmit}
        initialValues={initialValuesData}
        submitLoading={loading}
      />
    </div>
  )
}

export default EditCountry
