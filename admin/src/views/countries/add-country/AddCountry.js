import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { CountryForm } from '../country-form'
import { useAdminCountries } from '../../../common';

const AddCountry = () => {
  const history = useHistory();

  const [{ loading, data }, addCountry] = useAdminCountries({ method: 'POST' }, { manual: true })

  const onSubmit = (values) => {
    addCountry({
      data: values
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/countries");
    }
  }, [data, history])

  const initialValues = {
    country: { value: 1, label: 'syria' }
  }

  return (
    <div className="add-countries">
      <CountryForm
        onSubmit={onSubmit}
        initialValues={initialValues}
        submitLoading={loading}
      />
    </div>
  )
}

export default AddCountry
