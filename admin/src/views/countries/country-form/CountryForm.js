import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form';
import { Field, Btn } from '../../../common';
import withSelectOptions from './withSelectOptions';
import { useParams } from 'react-router-dom';

const CountryForm = ({
  onSubmit = () => {},
  submitLoading: loading = false,
  initialValues,
}) => {
  const { countryId } = useParams();
  
  const { control, handleSubmit, errors, reset } = useForm({ shouldFocusError: true });
  
  useEffect(() => {
    if (countryId) reset(initialValues)
  }, [initialValues]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Field
        name="code"
        placeholder="Enter country code (ex: SY)"
        control={control}
        errors={errors}
      />
      <Field
        name="name.en"
        placeholder="Enter country name in (English)"
        control={control}
        errors={errors}
      />
      <Field
        name="name.ar"
        placeholder="ادخل اسم المدينة (بالعربي)"
        control={control}
        errors={errors}
      />
      <Btn
        variant="primary"
        type="submit"
        loading={loading}
      >{countryId ? 'Edit Country' : 'Add Country'}</Btn>
    </form>
  )
}

export default withSelectOptions(CountryForm)
