import React, { useState } from 'react'
import { useCities, useCountries, useNeighborhoods } from '../../../common';
import { bathroomsOptions, bedroomsOptions, formatOptionsWithLang } from '../../../components/config';

const withSelectOptions = Component => ({
  ...props
}) => {
  const [city, setCity] = useState();
  
  const { data: countries, loading: loadingCountries } = useCountries();
  const countriesOptions = formatOptionsWithLang(countries);

  const { data: cities, loading: loadingCities } = useCities();
  const citiesOptions = formatOptionsWithLang(cities);

  const { data: neighborhoods, loading: loadingNeighborhoods } = useNeighborhoods({ perPage: 10000, filters: city ? { cityId: city } : undefined });
  const neighborhoodsOptions = formatOptionsWithLang(neighborhoods);

  return (
    <Component
      {...props}
      onChange={setCity}
      countries={{ options: countriesOptions, loading: loadingCountries }}
      cities={{ options: citiesOptions, loading: loadingCities }}
      neighborhoods={{ options: neighborhoodsOptions, loading: loadingNeighborhoods }}
      bedrooms={{ options: bedroomsOptions, loading: false }}
      bathrooms={{ options: bathroomsOptions, loading: false }}
    />
  )
}

export default withSelectOptions
