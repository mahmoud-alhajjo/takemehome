import React, { useEffect } from 'react';
import { addNewPropertyFormatter, citiesLocations, getPropertyFormatter, removeImagePrefix } from '../../../components/helpers';
import { useHistory, useParams } from 'react-router-dom';
import { PropertyForm } from '../property-form'
import { useAdminProperties } from '../../../common';

const EditProperty = () => {
  const history = useHistory();
  const { propertyId } = useParams();

  const [{ loading, data }, editProperty] = useAdminProperties({ method: 'POST', id: propertyId }, { manual: true });
  const [{ data: { content: { data: initialValuesData } = {} } = {} }] = useAdminProperties({ id: propertyId });
  const init = getPropertyFormatter(initialValuesData);

  const onSubmit = (values) => {
    console.log(values);
    editProperty({
      data: addNewPropertyFormatter({
        ...values,
        _method: 'PUT',
        deletedImages: values.deletedImages?.map(dImg => removeImagePrefix(dImg)) || '[]',
        images: values.images?.filter(img => typeof img !== 'string')
      })
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/properties");
    }
  }, [data]);

  return (
    <div className="edit-properties">
      <PropertyForm
        onSubmit={onSubmit}
        initialValues={init}
        submitLoading={loading}
      />
    </div>
  )
}

export default EditProperty
