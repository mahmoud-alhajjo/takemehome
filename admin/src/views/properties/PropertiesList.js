import React, { useEffect, useState } from 'react';
import { Badge, Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import { Endpoints, useAdminProperties } from '../../common';
import { dtoToDatatable } from '../../common/helpers';
import { Table } from '../../components';
import { toQueryStringObject } from '../../components/helpers';
import { useDidMountEffect } from '../../components/hooks';
import { propertyDto } from '../../dto';
import { dateCustomColumns } from '../../utils';

const PropertiesList = () => {
  const [{
    data: {
      content = {},
    } = {},
    loading
  } = {}, refetch] = useAdminProperties();
  const [search, setSearch] = useState();
  const filters = toQueryStringObject({ filters: { search } });
  const { data: properties, paginate = {} } = content || {};

  useDidMountEffect(() => {
    refetch({
      params: { page: 1, ...(filters || {})}
    })
  }, [search])

  const paginationProps = {
    manual: true,
    pages: paginate.lastPage,
    defaultPageSize: 10,
    onPageChange: (page) => {
      refetch({
        params: { page: page + 1, ...(filters || {}) }
      })
    },
    onPageSizeChange: (perPage, page) => {
      refetch({
        params: { perPage, page: page + 1, ...(filters || {}) }
      })
    },
  }

  return (
    <div className="properties-list">
      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText><i className="icon-magnifier" /></InputGroupText>
        </InputGroupAddon>
        <Input placeholder="Search" onChange={({ target: { value } = {} }) => setSearch(value)} />
      </InputGroup>
      <Table
        data={properties}
        columns={dtoToDatatable(propertyDto, customColumns)}
        loading={loading}
        {...paginationProps}
        actions={{
          props: {
            width: 200
          },
          delete: {
            onSuccess: () => refetch({
              params: { page: 1, ...(filters || {})}
            }),
            urlPrefix: Endpoints.admin.properties
          },
          accept: {
            onSuccess: () => refetch({
              params: { page: 1, ...(filters || {})}
            }),
            urlPrefix: Endpoints.admin.properties
          },
          edit: {
            urlPrefix: '/edit-property'
          }
        }}
      />
    </div>
  )
}

const customColumns = {
  amenities: {
    Cell: ({ value }) => (
      <span>{Object.keys(value).join(', ')}</span>
    )
  },
  contractType: {
    Cell: ({ value }) => <Badge color={value === 'sell' ? "primary" : "warning"} className="size-lg">{value}</Badge>
  },
  countryName: {
    Cell: ({ value }) => <span>{value?.['en']} ({value?.['ar']})</span>
  },
  location: {
    Cell: ({ value }) => <span>({value?.['lat']} {value?.['lng']})</span>
  },
  cityName: {
    Cell: ({ value }) => <span>{value?.['en']} ({value?.['ar']})</span>
  },
  neighbourhoodName: {
    Cell: ({ value }) => <span>{value?.['en']} ({value?.['ar']})</span>
  },
  ...dateCustomColumns
}

export default PropertiesList
