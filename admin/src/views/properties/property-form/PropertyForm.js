import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { Dropzone } from '../../../components/dropzone';
import { Maps } from '../../../components/maps';
import { Button } from 'reactstrap';
import { Field, Btn } from '../../../common';
import withSelectOptions from './withSelectOptions';
import {
  priceSellMarks,
  priceRentMarks,
  priceRentMax,
  priceSellMax,
  spaceMarks,
  spaceMax,
  amenities
} from '../../../components/config';
import Slider from 'rc-slider';
import Select from 'react-select';
import SelectCreatable from 'react-select/creatable';
import { numberFormatter, imageConverter, imageFallback, debounce } from '../../../components/helpers';
import { useParams } from 'react-router-dom';
import ImageGallery from 'react-image-gallery';

const PropertyForm = ({
  onSubmit = () => {},
  onChange = () => {},
  submitLoading: loading = false,
  initialValues = {},
  countries = {},
  cities = {},
  neighborhoods = {},
  bedrooms = {},
  bathrooms = {}
}) => {
  const { propertyId } = useParams();
  const [values, setValues] = useState(initialValues);

  const { control, handleSubmit, errors, watch, reset } = useForm({ shouldFocusError: true });
  const city = watch('city')?.value;
  
  useEffect(() => {
    onChange(city);
  }, [city]);
  
  useEffect(() => {
    if (propertyId) {
      setValues(v => ({ ...v, ...(initialValues || {}) }));
      reset({
        ...(initialValues || {}),
        country: countries.options?.find(({ value }) => value === initialValues?.country), // { value: 1, label: t('syria') },
        city: cities.options?.find(({ value }) => value === (city || initialValues?.city)),
        neighborhood: neighborhoods.options?.find(({ value }) => value === initialValues?.neighborhood)
      })
    }
  }, [initialValues, neighborhoods, countries, cities, propertyId]);

  const imageGalleryProps = propertyId ? {
    renderItem: item => {
      return (
        <div className="img-container">
          {!item.original?.startsWith?.('blob:') &&
            <Button close className="py-1 px-2 bg-danger text-light" onClick={() => setValues(v => ({ ...v, deletedImages: (v.deletedImages || [])?.concat?.([item.original]), images: v.images?.filter?.(img => img !== item.original) }))} />}
          <img src={item.original} onError={imageFallback} />
        </div>
      )
    }
  } : {}

  return (
    <form onSubmit={handleSubmit(formValues => onSubmit({ ...values, ...formValues }))}>
      <div className="row justify-content-center">
        <div className="col-xl-9 col-lg-10">
          <div className="row mb-5">
            <div className="col-5">
              <div className="section-title mb-md-0">
                <h4 className="pt-lg-1 pt-2">Category</h4>
              </div>
            </div>
            <div className="col-7 text-right add-property-btn-wrap">
              <Button
                color={values?.category === "sell" ? "primary" : "secondary"}
                className="mr-md-3"
                onClick={() => setValues(v => ({ ...v, category: 'sell' }))}
              >Sell</Button>
              <Button
                color={values?.category === "rent" ? "primary" : "secondary"}
                onClick={() => setValues(v => ({ ...v, category: 'rent' }))}
              >Rent</Button>
            </div>
          </div>
          <div className="row pd-top-100">
            <div className="col-md-4">
              <div className="section-title">
                <h4>Images</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-lg-12 mb-3">
                  <Dropzone
                    onDrop={images => setValues(v => ({ ...v, images }))}
                  />
                </div>
                <div className="col-lg-12 mb-3">
                  {values.images && values.images.length > 0 && (
                    <ImageGallery
                      items={values.images?.map?.(img => ({ original: imageConverter(img), thumbnail: imageConverter(img) })) || []}
                      showThumbnails={values.images?.length > 1}
                      showPlayButton={values.images?.length > 1}
                      {...imageGalleryProps}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-100">
            <div className="col-md-4">
              <div className="section-title">
                <h4>House Description</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-lg-12 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="title"
                      placeholder="Title"
                      control={control}
                      rules={{ required: true }}
                      errors={errors}
                    />
                  </div>
                </div>
                <div className="col-lg-12 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="description"
                      placeholder="Description"
                      tag="textarea"
                      rows={5}
                      control={control}
                      rules={{ required: true }}
                      errors={errors}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-100">
            <div className="col-md-4">
              <div className="section-title">
                <h4>House Information</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-lg-6 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="bedrooms"
                      component={Select}
                      placeholder="Bedrooms"
                      options={bedrooms.options}
                      control={control}
                      rules={{ required: true }}
                      errors={errors}
                    />
                  </div>
                </div>
                <div className="col-lg-6 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="bathrooms"
                      component={Select}
                      placeholder="Bathrooms"
                      options={bathrooms.options}
                      control={control}
                      rules={{ required: true }}
                      errors={errors}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-80">
            <div className="col-md-4">
              <div className="section-title">
                <h4>Area</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-12 mb-3">
                  <div className="rld-single-select mb-3">
                    <Slider
                      min={0}
                      max={spaceMax}
                      defaultValue={values.space}
                      className="mb-3"
                      marks={spaceMarks}
                      onChange={debounce(space => setValues(v => ({ ...v, space })), 500)}
                    />
                  </div>
                  <span className="d-inline-block mt-3">{values.space} Sq</span>
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-80">
            <div className="col-md-4">
              <div className="section-title">
                <h4>Address</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-lg-6 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="country"
                      component={Select}
                      placeholder="Country"
                      isLoading={countries.loading}
                      options={countries.options}
                      control={control}
                      rules={{ required: true }}
                      errors={errors}
                    />
                  </div>
                </div>
                <div className="col-lg-6 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="city"
                      component={Select}
                      placeholder="City"
                      isLoading={cities.loading}
                      options={cities.options}
                      control={control}
                      rules={{ required: true }}
                      errors={errors}
                    />
                  </div>
                </div>
                <div className="col-lg-12 mb-3">
                  <div className="rld-single-select">
                    <Field
                      name="neighborhood"
                      component={Select}
                      placeholder="Neighborhood"
                      isLoading={neighborhoods.loading}
                      options={neighborhoods.options}
                      control={control}
                      rules={{ required: true }}
                      errors={errors}
                    />
                  </div>
                </div>
                {/* <div className="col-12 mb-2">
                  <Field
                    name="address"
                    placeholder="Address"
                    tag="textarea"
                    rows={5}
                    control={control}
                    errors={errors}
                  />
                </div> */}
                <div className="col-12 mb-5">
                  <Maps 
                    onChange={location => setValues(v => ({ ...v, location }))} 
                    initCoords={{ lat: values.location?.lat, lng: values.location?.lng }} 
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-80">
            <div className="col-md-4">
              <div className="section-title">
                <h4>Price</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-12 mb-3">
                  <div className="rld-single-select mb-3">
                    <Slider
                      min={0}
                      max={values.category === 'sell' ? priceSellMax : priceRentMax}
                      defaultValue={values.price}
                      className="mb-3"
                      marks={values.category === 'sell' ? priceSellMarks : priceRentMarks}
                      onChange={debounce(price => setValues(v => ({ ...v, price })), 500)}
                    />
                  </div>
                  <span className="d-inline-block mt-3">{numberFormatter(values.price)}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="row pd-top-80">
            <div className="col-md-4">
              <div className="section-title">
                <h4>Amenities</h4>
              </div>
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-12">
                  <p>Choose house amenities</p>
                  <Field
                    name="amenities"
                    isClearable
                    isMulti
                    component={SelectCreatable}
                    control={control}
                    errors={errors}
                    rules={{ required: true }}
                    options={amenities.map(amenity => ({ label: amenity, value: amenity }))}
                  />
                </div>
                <div className="col-12 my-5">
                  <Btn
                    variant="primary"
                    type="submit"
                    loading={loading}
                  >{propertyId ? 'Edit Property' : 'Add Property'}</Btn>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  )
}

export default withSelectOptions(PropertyForm)
