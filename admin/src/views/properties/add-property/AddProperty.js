import React, { useEffect } from 'react';
import { addNewPropertyFormatter, citiesLocations } from '../../../components/helpers';
import { useHistory } from 'react-router-dom';
import { PropertyForm } from '../property-form'
import { useAdminProperties } from '../../../common';

const AddProperty = () => {
  const history = useHistory();

  const [{ loading, data }, addProperty] = useAdminProperties({ method: 'POST' }, { manual: true })

  const onSubmit = (values) => {
    addProperty({
      data: addNewPropertyFormatter(values)
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/properties");
    }
  }, [data, history])

  const initialValues = {
    category: 'sell',
    price: 100000000,
    space: 100,
    amenities: []
  }

  return (
    <div className="edit-properties">
      <PropertyForm
        onSubmit={onSubmit}
        initialValues={initialValues}
        submitLoading={loading}
      />
    </div>
  )
}

export default AddProperty
