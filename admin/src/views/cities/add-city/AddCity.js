import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { CityForm } from '../city-form'
import { useAdminCities } from '../../../common';

const AddCity = () => {
  const history = useHistory();

  const [{ loading, data }, addCity] = useAdminCities({ method: 'POST' }, { manual: true })

  const onSubmit = (values) => {
    addCity({
      data: {
        countryId: values.country?.value,
        name: values.name
      }
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/cities");
    }
  }, [data, history])

  const initialValues = {
    country: { value: 1, label: 'syria' }
  }

  return (
    <div className="add-cities">
      <CityForm
        onSubmit={onSubmit}
        initialValues={initialValues}
        submitLoading={loading}
      />
    </div>
  )
}

export default AddCity
