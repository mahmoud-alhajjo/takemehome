import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { CityForm } from '../city-form'
import { useAdminCities } from '../../../common';

const EditCity = () => {
  const history = useHistory();
  const { cityId } = useParams();

  const [{ loading, data }, editCity] = useAdminCities({ method: 'POST', id: cityId }, { manual: true });
  const [{ loading: initialValuesLoading, data: { content: { data: initialValuesData } = {} } = {} }] = useAdminCities({ id: cityId });

  const onSubmit = (values) => {
    editCity({
      data: values
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/cities");
    }
  }, [data]);

  return (
    <div className="edit-cities">
      <CityForm
        onSubmit={onSubmit}
        initialValues={initialValuesData}
        submitLoading={loading}
      />
    </div>
  )
}

export default EditCity
