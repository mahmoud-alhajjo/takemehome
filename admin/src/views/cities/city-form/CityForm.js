import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form';
import { Field, Btn } from '../../../common';
import withSelectOptions from './withSelectOptions';
import Select from 'react-select';
import { useParams } from 'react-router-dom';

const CityForm = ({
  onSubmit = () => {},
  submitLoading: loading = false,
  initialValues,
  countries = {}
}) => {
  const { cityId } = useParams();
  
  const { control, handleSubmit, errors, reset } = useForm({ shouldFocusError: true });
  
  useEffect(() => {
    if (cityId) reset({
      name: initialValues?.name,
      country: countries.options?.find(option => option.value === initialValues?.countryId)
    })
  }, [initialValues, countries.options]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Field
        name="country"
        component={Select}
        placeholder="Choose Country"
        isLoading={countries.loading}
        options={countries.options}
        control={control}
        rules={{ required: true }}
        errors={errors}
      />
      <Field
        name="name.en"
        placeholder="Enter city name in (English)"
        control={control}
        errors={errors}
      />
      <Field
        name="name.ar"
        placeholder="ادخل اسم المدينة (بالعربي)"
        control={control}
        errors={errors}
      />
      <Btn
        variant="primary"
        type="submit"
        loading={loading}
      >{cityId ? 'Edit City' : 'Add City'}</Btn>
    </form>
  )
}

export default withSelectOptions(CityForm)
