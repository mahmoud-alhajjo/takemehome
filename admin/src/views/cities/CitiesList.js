import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { Endpoints, useAdminCities } from '../../common';
import { dtoToDatatable } from '../../common/helpers';
import { Table } from '../../components';
import { cityDto } from '../../dto';
import { dateCustomColumns } from '../../utils';

const CitiesList = () => {
  const [{ data: { content: { data: cities } = {} } = {}, loading }, refetch] = useAdminCities();

  return (
    <div className="cities-list">
      <div className="d-flex justify-content-end mb-3">
        <Button color="primary" tag={Link} to="/add-city">Add new city</Button>
      </div>
      <Table
        data={cities}
        columns={dtoToDatatable(cityDto, customColumns)}
        loading={loading}
        actions={{ 
          delete: { 
            onSuccess: () => refetch(), 
            urlPrefix: Endpoints.admin.cities 
          }, 
          edit: { 
            urlPrefix: '/edit-city' 
          }, 
          accept: { 
            hidden: true 
          } 
        }}
      />
    </div>
  )
}

const customColumns = { 
  name: { 
    Cell: ({ value }) => <span>{value.en} ({value.ar})</span>
  },
  countryName: {
    Cell: ({ value }) => <span>{value?.['en']} ({value?.['ar']})</span>
  },
  ...dateCustomColumns
}

export default CitiesList
