import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { SettingForm } from '../setting-form'
import { useAdminSettings } from '../../../common';

const EditSetting = () => {
  const history = useHistory();
  const { settingId } = useParams();

  const [{ loading, data }, editSetting] = useAdminSettings({ method: 'PUT', id: settingId }, { manual: true });
  const [{ loading: initialValuesLoading, data: { content: { data: initialValuesData } = {} } = {} }] = useAdminSettings({ id: settingId });

  const onSubmit = (values) => {
    editSetting({
      data: values
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/settings");
    }
  }, [data]);

  return (
    <div className="edit-settings">
      <SettingForm
        onSubmit={onSubmit}
        initialValues={initialValuesData}
        submitLoading={loading}
      />
    </div>
  )
}

export default EditSetting
