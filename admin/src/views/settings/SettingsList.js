import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { Endpoints, useAdminSettings } from '../../common';
import { dtoToDatatable } from '../../common/helpers';
import { Table } from '../../components';
import { settingDto } from '../../dto';
import { dateCustomColumns } from '../../utils';

const SettingsList = () => {
  const [{ data: { content: { data: settings } = {} } = {}, loading }, refetch] = useAdminSettings();

  return (
    <div className="settings-list">
      <div className="d-flex justify-content-end mb-3">
        <Button color="primary" tag={Link} to="/add-setting">Add new setting</Button>
      </div>
      <Table
        data={settings}
        columns={dtoToDatatable(settingDto, customColumns)}
        loading={loading}
        actions={{ 
          delete: { 
            onSuccess: () => refetch(), 
            urlPrefix: Endpoints.admin.settings 
          }, 
          edit: { 
            urlPrefix: '/edit-setting' 
          }, 
          accept: { 
            hidden: true 
          } 
        }}
      />
    </div>
  )
}

const customColumns = { 
  name: { 
    Cell: ({ value }) => <span>{value.en} ({value.ar})</span>
  },
  ...dateCustomColumns
}

export default SettingsList
