import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { SettingForm } from '../setting-form'
import { useAdminSettings } from '../../../common';

const AddSetting = () => {
  const history = useHistory();

  const [{ loading, data }, addSetting] = useAdminSettings({ method: 'POST' }, { manual: true })

  const onSubmit = (values) => {
    addSetting({
      data: values
    })
  }

  useEffect(() => {
    if (data?.code < 300) {
      history.push("/settings");
    }
  }, [data, history])

  const initialValues = {
    setting: { value: 1, label: 'syria' }
  }

  return (
    <div className="add-settings">
      <SettingForm
        onSubmit={onSubmit}
        initialValues={initialValues}
        submitLoading={loading}
      />
    </div>
  )
}

export default AddSetting
