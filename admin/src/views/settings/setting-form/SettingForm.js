import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form';
import { Field, Btn } from '../../../common';
import { useParams } from 'react-router-dom';

const SettingForm = ({
  onSubmit = () => {},
  submitLoading: loading = false,
  initialValues,
  cities = {}
}) => {
  const { settingId } = useParams();
  
  const { control, handleSubmit, errors, reset } = useForm({ shouldFocusError: true });
  
  useEffect(() => {
    if(settingId) reset(initialValues)
  }, [initialValues, cities.options]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Field
        name="key"
        placeholder="Enter setting key"
        control={control}
        errors={errors}
      />
      <Field
        name="value"
        tag="textarea"
        rows={5}
        placeholder="Enter setting value"
        control={control}
        errors={errors}
      />
      <Btn
        variant="primary"
        type="submit"
        loading={loading}
      >{settingId ? 'Edit Setting' : 'Add Setting'}</Btn>
    </form>
  )
}

export default SettingForm
