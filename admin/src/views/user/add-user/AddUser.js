import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { UserForm } from '../user-form'
import { useAdminUsers } from '../../../common';

const AddUser = () => {
  const history = useHistory();

  const [{ loading, data }, addUser] = useAdminUsers({ method: 'POST' }, { manual: true })

  const onSubmit = (values) => {
    addUser({
      data: values
    })
  }

  useEffect(() => {
    if (data?.code < 300) {
      history.push("/users");
    }
  }, [data, history])

  const initialValues = {
    user: { value: 1, label: 'syria' }
  }

  return (
    <div className="add-users">
      <UserForm
        onSubmit={onSubmit}
        initialValues={initialValues}
        submitLoading={loading}
      />
    </div>
  )
}

export default AddUser
