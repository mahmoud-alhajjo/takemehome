import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form';
import { Field, Btn } from '../../../common';
import { useParams } from 'react-router-dom';

const UserForm = ({
  onSubmit = () => {},
  submitLoading: loading = false,
  initialValues,
  cities = {}
}) => {
  const { userId } = useParams();
  
  const { control, handleSubmit, errors, reset } = useForm({ shouldFocusError: true });
  
  useEffect(() => {
    reset(initialValues)
  }, [initialValues, cities.options]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Field
        name="key"
        placeholder="Enter user key"
        control={control}
        errors={errors}
      />
      <Field
        name="value"
        tag="textarea"
        rows={5}
        placeholder="Enter user value"
        control={control}
        errors={errors}
      />
      <Btn
        variant="primary"
        type="submit"
        loading={loading}
      >{userId ? 'Edit User' : 'Add User'}</Btn>
    </form>
  )
}

export default UserForm
