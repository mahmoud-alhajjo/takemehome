import React from 'react';
import { Endpoints, timeAgo, useAdminUsers } from '../../common';
import { dtoToDatatable } from '../../common/helpers';
import { Table } from '../../components';
import { userDto } from '../../dto';
import { dateCustomColumns } from '../../utils';

const UsersList = () => {
  const [{ data: { content: { data: users } = {} } = {}, loading }, refetch] = useAdminUsers();

  return (
    <div className="users-list">
      <Table
        data={users}
        columns={dtoToDatatable(userDto, customColumns)}
        loading={loading}
        actions={{ 
          enable: {
            hidden: false,
            urlPrefix: Endpoints.admin.enableUsers,
            onSuccess: () => refetch()
          },
          disable: {
            hidden: false,
            urlPrefix: Endpoints.admin.disableUsers,
            onSuccess: () => refetch()
          },
          delete: { hidden: true }, 
          edit: { hidden: true }, 
          accept: { hidden: true } 
        }}
      />
    </div>
  )
}

const customColumns = {
  ...dateCustomColumns,
  enabledAt: { Cell: ({ value }) => <span>{timeAgo(value)}</span> },
  verifiedAt: { Cell: ({ value }) => <span>{timeAgo(value)}</span> }
}

export default UsersList
