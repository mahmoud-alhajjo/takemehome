import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { UserForm } from '../user-form'
import { useAdminUsers } from '../../../common';

const EditUser = () => {
  const history = useHistory();
  const { userId } = useParams();

  const [{ loading, data }, editUser] = useAdminUsers({ method: 'PUT', id: userId }, { manual: true });
  const [{ loading: initialValuesLoading, data: { content: { data: initialValuesData } = {} } = {} }] = useAdminUsers({ id: userId });

  const onSubmit = (values) => {
    editUser({
      data: values
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/users");
    }
  }, [data]);

  return (
    <div className="edit-users">
      <UserForm
        onSubmit={onSubmit}
        initialValues={initialValuesData}
        submitLoading={loading}
      />
    </div>
  )
}

export default EditUser
