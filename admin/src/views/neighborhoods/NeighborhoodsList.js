import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { Endpoints, useAdminNeighborhoods } from '../../common';
import { dtoToDatatable } from '../../common/helpers';
import { Table } from '../../components';
import { neighborhoodDto } from '../../dto';
import { dateCustomColumns } from '../../utils';

const NeighborhoodsList = () => {
  const [{ data: { content: { data: neighborhoods } = {} } = {}, loading }, refetch] = useAdminNeighborhoods();

  return (
    <div className="neighborhoods-list">
      <div className="d-flex justify-content-end mb-3">
        <Button color="primary" tag={Link} to="/add-neighborhood">Add new neighborhood</Button>
      </div>
      <Table
        data={neighborhoods}
        columns={dtoToDatatable(neighborhoodDto, customColumns)}
        loading={loading}
        actions={{ 
          delete: { 
            onSuccess: () => refetch(), 
            urlPrefix: Endpoints.admin.neighborhoods 
          }, 
          edit: { 
            urlPrefix: '/edit-neighborhood' 
          }, 
          accept: { 
            hidden: true 
          } 
        }}
      />
    </div>
  )
}

const customColumns = { 
  name: { 
    Cell: ({ value }) => <span>{value.en} ({value.ar})</span>
  },
  cityName: {
    Cell: ({ value }) => <span>{value?.['en']} ({value?.['ar']})</span>
  },
  ...dateCustomColumns
}

export default NeighborhoodsList
