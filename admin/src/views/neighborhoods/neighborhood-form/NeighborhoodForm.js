import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form';
import { Field, Btn } from '../../../common';
import withSelectOptions from './withSelectOptions';
import { useParams } from 'react-router-dom';
import Select from 'react-select';

const NeighborhoodForm = ({
  onSubmit = () => {},
  submitLoading: loading = false,
  initialValues,
  cities = {}
}) => {
  const { neighborhoodId } = useParams();
  
  const { control, handleSubmit, errors, reset } = useForm({ shouldFocusError: true });
  
  useEffect(() => {
    if (neighborhoodId) reset({
      ...(initialValues || {}),
      city: cities.options?.find(option => option.value === initialValues?.cityId)
    })
  }, [initialValues, cities.options]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Field
        name="city"
        component={Select}
        placeholder="Choose city"
        isLoading={cities.loading}
        options={cities.options}
        control={control}
        rules={{ required: true }}
        errors={errors}
      />
      <Field
        name="name.en"
        placeholder="Enter neighborhood name in (English)"
        control={control}
        errors={errors}
      />
      <Field
        name="name.ar"
        placeholder="ادخل اسم المدينة (بالعربي)"
        control={control}
        errors={errors}
      />
      <Btn
        variant="primary"
        type="submit"
        loading={loading}
      >{neighborhoodId ? 'Edit Neighborhood' : 'Add Neighborhood'}</Btn>
    </form>
  )
}

export default withSelectOptions(NeighborhoodForm)
