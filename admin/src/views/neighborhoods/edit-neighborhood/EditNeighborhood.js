import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { NeighborhoodForm } from '../neighborhood-form'
import { useAdminNeighborhoods } from '../../../common';

const EditNeighborhood = () => {
  const history = useHistory();
  const { neighborhoodId } = useParams();

  const [{ loading, data }, editNeighborhood] = useAdminNeighborhoods({ method: 'POST', id: neighborhoodId }, { manual: true });
  const [{ loading: initialValuesLoading, data: { content: { data: initialValuesData } = {} } = {} }] = useAdminNeighborhoods({ id: neighborhoodId });

  const onSubmit = (values) => {
    editNeighborhood({
      data: values
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/neighborhoods");
    }
  }, [data]);

  return (
    <div className="edit-neighborhoods">
      <NeighborhoodForm
        onSubmit={onSubmit}
        initialValues={initialValuesData}
        submitLoading={loading}
      />
    </div>
  )
}

export default EditNeighborhood
