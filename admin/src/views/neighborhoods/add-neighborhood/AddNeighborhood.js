import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { NeighborhoodForm } from '../neighborhood-form'
import { useAdminNeighborhoods } from '../../../common';

const AddNeighborhood = () => {
  const history = useHistory();

  const [{ loading, data }, addNeighborhood] = useAdminNeighborhoods({ method: 'POST' }, { manual: true })

  const onSubmit = (values) => {
    addNeighborhood({
      data: values
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/neighborhoods");
    }
  }, [data, history])

  const initialValues = {
    neighborhood: { value: 1, label: 'syria' }
  }

  return (
    <div className="add-neighborhoods">
      <NeighborhoodForm
        onSubmit={onSubmit}
        initialValues={initialValues}
        submitLoading={loading}
      />
    </div>
  )
}

export default AddNeighborhood
