import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { ArticleForm } from '../article-form'
import { useAdminArticles } from '../../../common';
import { toFormData } from '../../../components/helpers';

const EditArticle = () => {
  const history = useHistory();
  const { articleId } = useParams();

  const [{ loading, data }, editArticle] = useAdminArticles({ method: 'PUT', id: articleId }, { manual: true });
  const [{ loading: initialValuesLoading, data: { content: { data: initialValuesData } = {} } = {} }] = useAdminArticles({ id: articleId });

  const onSubmit = (values) => {
    editArticle({
      data: toFormData({
        ...values,
        image: values.image || undefined
      })
    })
  }

  useEffect(() => {
    if (data?.code === 200) {
      history.push("/articles");
    }
  }, [data]);

  return (
    <div className="edit-articles">
      <ArticleForm
        onSubmit={onSubmit}
        initialValues={initialValuesData}
        submitLoading={loading}
      />
    </div>
  )
}

export default EditArticle
