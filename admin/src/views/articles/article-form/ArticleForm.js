import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form';
import { Field, Btn } from '../../../common';
import { Dropzone } from '../../../components/dropzone'
import { useParams } from 'react-router-dom';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { imageConverter, imageFallback, withImagePrefix } from '../../../components/helpers';

const ArticleForm = ({
  onSubmit = () => { },
  submitLoading: loading = false,
  initialValues
}) => {
  const { articleId } = useParams();

  const { control, handleSubmit, errors, reset, setValue, watch } = useForm({ shouldFocusError: true });

  useEffect(() => {
    if (articleId) reset(initialValues)
  }, [initialValues]);

  const image = watch('image');

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Field
        name="image"
        label="Image"
        placeholder="Enter article image"
        component={Dropzone}
        onDrop={image => image && setValue('image', image[0])}
        control={control}
        errors={errors}
      />
      <div className="col-lg-12 mb-3">
        {image && 
          <img 
            src={imageConverter(image).startsWith('blob') ? imageConverter(image) : withImagePrefix(imageConverter(image))} 
            className="d-block mx-auto w-100" 
            onError={imageFallback} 
          />}
      </div>
      <Field
        name="name"
        label="Name"
        placeholder="Enter article title"
        rules={{ required: true }}
        control={control}
        errors={errors}
      />
      <Field
        name="description"
        label="Description"
        placeholder="Enter article description"
        rules={{ required: true }}
        control={control}
        errors={errors}
      />
      <Field
        name="content"
        hidden
        placeholder="Enter article content"
        control={control}
        errors={errors}
      />
      <CKEditor
        editor={ClassicEditor}
        data={initialValues?.content}
        onChange={(event, editor) => {
          setValue('content', editor.getData(), { shouldDirty: true });
        }}
      />
      <Btn
        variant="primary"
        type="submit"
        className="my-3"
        loading={loading}
      >{articleId ? 'Edit Article' : 'Add Article'}</Btn>
    </form>
  )
}

export default ArticleForm
