import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { Endpoints, useAdminArticles } from '../../common';
import { dtoToDatatable } from '../../common/helpers';
import { Table } from '../../components';
import { articleDto } from '../../dto';
import { dateCustomColumns } from '../../utils';

const ArticlesList = () => {
  const [{ data: { content: { data: articles } = {} } = {}, loading }, refetch] = useAdminArticles();

  return (
    <div className="articles-list">
      <div className="d-flex justify-content-end mb-3">
        <Button color="primary" tag={Link} to="/add-article">Add new article</Button>
      </div>
      <Table
        data={articles}
        columns={dtoToDatatable(articleDto, dateCustomColumns)}
        loading={loading}
        actions={{ 
          delete: { 
            onSuccess: () => refetch(), 
            urlPrefix: Endpoints.admin.articles 
          }, 
          edit: { 
            urlPrefix: '/edit-article' 
          }, 
          accept: { 
            hidden: true 
          } 
        }}
      />
    </div>
  )
}

export default ArticlesList
