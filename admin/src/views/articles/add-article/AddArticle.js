import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { ArticleForm } from '../article-form'
import { useAdminArticles } from '../../../common';
import { toFormData } from '../../../components/helpers';

const AddArticle = () => {
  const history = useHistory();

  const [{ loading, data }, addArticle] = useAdminArticles({ method: 'POST' }, { manual: true })

  const onSubmit = (values) => {
    addArticle({
      data: toFormData({
        ...values,
        image: values.image || undefined
      })
    })
  }

  useEffect(() => {
    if (data?.code < 300) {
      history.push("/articles");
    }
  }, [data, history])

  const initialValues = {
    article: { value: 1, label: 'syria' }
  }

  return (
    <div className="add-articles">
      <ArticleForm
        onSubmit={onSubmit}
        initialValues={initialValues}
        submitLoading={loading}
      />
    </div>
  )
}

export default AddArticle
