import React from 'react'
import ReactTable from 'react-table-v6'
import 'react-table-v6/react-table.css'
import Actions from './Actions'

const Table = ({
  data,
  columns,
  actions,
  ...props
}) => {
  return (
    <ReactTable
      data={data || []}
      className="bg-white"
      defaultPageSize={10}
      showPageSizeOptions
      columns={[
        ...columns,
        {
          Header: 'Actions',
          width: 150,
          Cell: ({ row }) => <Actions row={row} actions={actions} />,
          ...(actions?.props || {})
        }
      ]}
      {...props}
    />
  )
}

export default Table
