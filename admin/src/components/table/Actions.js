import React from 'react'
import { useHistory } from 'react-router-dom'
import { Button, ButtonGroup } from 'reactstrap'
import { acceptModal, deleteModal } from '../../common'

const deleteHandler = async (row, urlPrefix, onSuccess = () => {}) => {
  return deleteModal(`${urlPrefix}/${row.id}`).then(data => {
    if (data) onSuccess(data);
  });
}

const acceptHandler = async (row, urlPrefix, onSuccess = () => {}) => {
  return acceptModal(`${urlPrefix}/accept/${row.id}`).then(data => {
    if (data) onSuccess(data);
  });
}

const editHandler = (row, urlPrefix, history) => {
  history.push(`${urlPrefix}/${row.id}`);
}

const enableHandler = async (row, urlPrefix, onSuccess = () => {}) => {
  return acceptModal(`${urlPrefix}/${row.id}`, {
    confirmButtonText: 'Enable',
    title: 'Are you sure you want to enable this user ?',
    successMessage: 'User Enabled Successfully!'
  }).then(data => {
    if (data) onSuccess(data);
  });
}

const disableHandler = async (row, urlPrefix, onSuccess = () => {}) => {
  return acceptModal(`${urlPrefix}/${row.id}`, {
    confirmButtonText: 'Disable',
    title: 'Are you sure you want to disable this user ?',
    successMessage: 'User Disabled Successfully!'
  }).then(data => {
    if (data) onSuccess(data);
  });
}

const defaultActions = {
  delete: {
    label: 'Delete',
    handler: deleteHandler
  },
  edit: {
    label: 'Edit',
    handler: editHandler
  },
  accept: {
    label: 'Accept',
    handler: acceptHandler
  },
  enable: {
    label: 'Enable',
    handler: enableHandler
  },
  disable: {
    label: 'Disable',
    handler: disableHandler
  },
}

const Actions = ({ 
  row,
  actions = {}
}) => {
  const history = useHistory();

  actions = {
    delete: {
      urlPrefix: '',
      onSuccess: () => {},
      ...defaultActions.delete,
      ...(actions.delete || {})
    },
    accept: {
      urlPrefix: '',
      disabled: row?.status === 'accepted',
      onSuccess: () => {},
      ...defaultActions.accept,
      ...(actions.accept || {})
    },
    edit: {
      urlPrefix: '',
      ...defaultActions.edit,
      ...(actions.edit || {})
    },
    enable: {
      hidden: true,
      urlPrefix: '',
      disabled: !!row.enabledAt,
      onSuccess: () => {},
      ...defaultActions.enable,
      ...(actions.enable || {})
    },
    disable: {
      hidden: true,
      urlPrefix: '',
      disabled: !row.enabledAt,
      onSuccess: () => {},
      ...defaultActions.disable,
      ...(actions.disable || {})
    }
  }

  const editAction = actions['edit'];
  const deleteAction = actions['delete'];
  const acceptAction = actions['accept'];
  const disableAction = actions['disable'];
  const enableAction = actions['enable'];

  return (
    <ButtonGroup>
      {(editAction && !editAction.hidden) && 
        <Button 
          color="secondary" 
          hidden={!!editAction.hidden}
          onClick={e => editAction.handler(row, editAction.urlPrefix, history)}
        >{editAction.label}</Button>}
      {(deleteAction && !deleteAction.hidden) && 
        <Button 
          color="danger" 
          hidden={!!deleteAction.hidden}
          onClick={e => deleteAction.handler(row, deleteAction.urlPrefix, deleteAction.onSuccess)}
        >{deleteAction.label}</Button>}
      {(acceptAction && !acceptAction.hidden) && 
        <Button 
          color="info" 
          hidden={!!acceptAction.hidden}
          disabled={acceptAction.disabled}
          onClick={e => acceptAction.handler(row, acceptAction.urlPrefix, acceptAction.onSuccess)}
        >{acceptAction.label}</Button>}
      {(disableAction && !disableAction.hidden) && 
        <Button 
          color="danger" 
          hidden={!!disableAction.hidden}
          disabled={disableAction.disabled}
          onClick={e => disableAction.handler(row, disableAction.urlPrefix, disableAction.onSuccess)}
        >{disableAction.label}</Button>}
        {(enableAction && !enableAction.hidden) && 
          <Button 
            color="primary" 
            hidden={!!enableAction.hidden}
            disabled={enableAction.disabled}
            onClick={e => enableAction.handler(row, enableAction.urlPrefix, enableAction.onSuccess)}
          >{enableAction.label}</Button>}
    </ButtonGroup>
  )
}

export default Actions
