import React, { useEffect, useState } from 'react'
import { GoogleMap, Marker, withGoogleMap, withScriptjs, DirectionsRenderer } from "react-google-maps"
import { debounce } from '../helpers';
import { citiesLocations } from './constants';

const Maps = ({
  onChange = () => null,
  draggable = true,
  initCoords,
  zoom = 10,
  ...props
}) => {
  const [coords, setCoords] = useState(initCoords);
  // const [directions, setDirections] = useState();
  
  useEffect(() => {
    setCoords(initCoords);
  }, [initCoords]);

  const handleChange = event => {
    if (draggable) {
      const crds = { lat: event.latLng.lat(), lng: event.latLng.lng() }
      setCoords(crds);
      onChange(crds);
    }
  }

  // useEffect(() => {
  //   const DirectionsService = new window.google.maps.DirectionsService();

  //   DirectionsService.route({
  //     origin: new window.google.maps.LatLng(41.8507300, -87.6512600),
  //     destination: new window.google.maps.LatLng(coords.lat, coords.lng),
  //     travelMode: window.google.maps.TravelMode.DRIVING,
  //   }, (result, status) => {
  //     if (status === window.google.maps.DirectionsStatus.OK) {
  //       setDirections(result);
  //     } else {
  //       console.error(`error fetching directions ${result}`);
  //     }
  //   });
  // }, [coords]);
  const crds = new window.google.maps.LatLng(coords?.lat ?? citiesLocations.damascus.lat, coords?.lng ?? citiesLocations.damascus.lng)
  return (
    <GoogleMap
      defaultZoom={zoom}
      center={crds}
    >
      {/* <DirectionsRenderer
        directions={directions}
      /> */}
      <Marker
        position={crds}
        draggable={draggable}
        onDrag={debounce(handleChange, 1000)}
      />
    </GoogleMap>
  )
}

const HocMaps = withScriptjs(
  withGoogleMap(Maps)
)

const MapsWrapper = ({
  googleMapURL = "https://maps.googleapis.com/maps/api/js?key=&v=3.exp&libraries=geometry,drawing,places",
  loadingElement = <div style={{ height: `100%` }} />,
  containerElement = <div style={{ height: `400px` }} />,
  mapElement = <div style={{ height: `100%` }} />,
  ...props
}) => {
  return (
    <HocMaps
      googleMapURL={googleMapURL}
      loadingElement={loadingElement}
      containerElement={containerElement}
      mapElement={mapElement}
      {...props}
    />
  )
}

export default MapsWrapper
