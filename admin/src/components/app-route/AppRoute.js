import React, { useEffect } from 'react'
import { Route, useHistory } from 'react-router-dom'
import { LocalStorage } from '../../common'

const AppRoute = ({
  exact = false,
  path,
  component,
  render,
  children,
  ...rest
}) => {
  const history = useHistory();
  const accessToken = LocalStorage.get('access_token');

  useEffect(() => {
    if (!accessToken) {
      history.push('/login');
    }
  }, [accessToken, history]);

  return (
    <Route exact path={path} {...rest}>
      {children || component || render}
    </Route>
  )
}

export default AppRoute
