import React, {useCallback, useState} from 'react'
import {useDropzone} from 'react-dropzone'

const Dropzone = ({
  onDrop: handleDrop = function() {}
}) => {
  const [files, setFiles] = useState();
  const onDrop = useCallback(acceptedFiles => {
    console.log(acceptedFiles);
    setFiles(acceptedFiles);
    handleDrop(acceptedFiles);
  }, [handleDrop]);

  const { 
    getRootProps, 
    getInputProps, 
    isDragActive
  } = useDropzone({ onDrop, accept: ['image/png', 'image/jpg', 'image/jpeg'] });

  return (
    <div className="dropzone" {...getRootProps()}>
      <input {...getInputProps()} />
      {
        files ? 
          files.map(file => <p key={JSON.stringify(file)}>{file.name}</p>)
        : isDragActive ?
          <p>Drop Images Here</p> :
          <p>Drag or Click to select images</p>
      }
    </div>
  )
}

export default Dropzone;
