export default {
  items: [
    // {
    //   name: 'Dashboard',
    //   url: '/dashboard',
    //   icon: 'icon-speedometer'
    // },
    {
      title: true,
      name: 'Users',
    },
    {
      name: 'Users',
      url: '/users',
      icon: 'icon-people',
    },
    {
      title: true,
      name: 'Properties',
    },
    {
      name: 'Properties',
      url: '/properties',
      icon: 'icon-home',
    },
    {
      name: 'Add Property',
      url: '/add-property',
      icon: 'icon-plus',
    },
    {
      title: true,
      name: 'Site Data',
    },
    {
      name: 'Countries',
      url: '/countries',
      icon: 'icon-map',
    },
    {
      name: 'Cities',
      url: '/cities',
      icon: 'icon-map',
    },
    {
      name: 'Neighborhoods',
      url: '/neighborhoods',
      icon: 'icon-map',
    },
    {
      title: true,
      name: 'Site Content',
    },
    {
      name: 'Settings',
      url: '/settings',
      icon: 'icon-settings',
    },
    {
      title: true,
      name: 'Blog',
    },
    {
      name: 'Articles',
      url: '/articles',
      icon: 'icon-docs',
    }
  ],
};
