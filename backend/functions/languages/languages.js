const arabic = require('./arabic.json');
const english = require('./english.json');

exports.handler = async function(event, context) {
  const { lang } = event.queryStringParameters || 'ar';
  const data = lang === 'en' ? english : arabic;

  return {
    body: JSON.stringify(data),
    statusCode: 200
  };
};